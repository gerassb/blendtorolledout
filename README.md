blendtorolledout
================

A Blender addon to create Rolled Out! stages in Blender.

# How do?

1. Download a zip of this repo

![Image of the download button](https://i.imgur.com/5XzzNGp.png)

2. Open Blender preferences and hit "Install addon from file" in the addons tab, and select the zip
3. Enable the addon in Blender
4. Look for the 'BlendToRolledOut' tab on the righthand side of the 3D viewport
