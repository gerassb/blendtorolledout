import bpy
import bpy_extras
from bpy.app.handlers import persistent
import importlib
import inspect

from bpy_extras import node_shader_utils
from .blendtorolledout import statics, stage_edit_mode_operator, menus, material_properties
from . import developer_utils
import traceback

bl_info = {
    "name": "blendtorolledout",
    "description": "Create Rolled Out! stage configs in Blender",
    "author": "CraftedCart, The BombSquad",
    "version": (0, 0, 2),
    "blender": (2, 91, 0),
    "location": "View3D",
    "warning": "Stage config formats are subject to change.",
    "wiki_url": "",
    "category": "3D View"
}

# Load and reload submodules
importlib.reload(developer_utils)
modules = developer_utils.setup_addon_modules(__path__, __name__, "bpy" in locals())

@persistent
def on_pre_frame_render(f):
    for mat in bpy.data.materials:
        for prop in (material_properties.mat_scalar_prop_name_list):
            update_mat(mat, bpy.context, prop)

@persistent
def on_load_file(f):
    statics.material_props.clear()
    for mat in bpy.data.materials:
        # Enable backface culling for all materials
        if hasattr(mat, 'use_backface_culling'):
            mat.use_backface_culling = True

        if (mat.use_nodes):
            nodes = mat.node_tree.nodes
            links = mat.node_tree.links

            material_output_node = None
            image_texture_node = None
            diffuse_node = None

            for node in nodes:
                if (node.type == 'BSDF_PRINCIPLED'):
                    continue
                elif (node.type =='OUTPUT_MATERIAL'):
                    material_output_node = node
                elif (node.type == 'TEX_IMAGE'):
                    image_texture_node = node
                elif (node.type =='BSDF_DIFFUSE'):
                    diffuse_node = node
                #else:
                    #print("Removing extraneous node" + str(node))
                    #nodes.remove(node)

            # Convert Diffuse BSDF nodes to Principled BSDF
            if diffuse_node is not None:
                print("Converting material " + mat.name + " diffuse BSDF node to principled BSDF...")
                nodes.remove(diffuse_node)
                principled_node = nodes.new("ShaderNodeBsdfPrincipled")
                links.new(material_output_node.inputs['Surface'], principled_node.outputs['BSDF'])
                links.new(principled_node.inputs['Base Color'], image_texture_node.outputs['Color'])
                principled_node.inputs['Specular'].default_value = 0.0

        # Sync initial Blender material props and Rolled Out material props
        print(f"Syncing material props for {mat.name}...")
        for prop in (material_properties.mat_scalar_prop_name_list):
            mat_wrap = node_shader_utils.PrincipledBSDFWrapper(mat, is_readonly=False)
            wrapper_attrib_name = material_properties.mat_props_wrapper_attribs[prop]
            setattr(mat_wrap, wrapper_attrib_name, getattr(mat, prop))

# Update function for Rolled Out materials
def update_mat(self, context, attrib_name):
    mat_wrap = node_shader_utils.PrincipledBSDFWrapper(self, is_readonly=False)
    wrapper_attrib_name = material_properties.mat_props_wrapper_attribs[attrib_name]

    wrapper_attrib = getattr(mat_wrap, wrapper_attrib_name)
    attrib = getattr(self, attrib_name)

    # Handles updating Blender material props on Rolled Out prop update
    if self.name not in statics.material_props:
        statics.material_props[self.name] = {}

    if attrib_name not in statics.material_props[self.name] or statics.material_props[self.name][attrib_name] != attrib:
        statics.material_props[self.name][attrib_name] = attrib 
        setattr(mat_wrap, wrapper_attrib_name, attrib) 
        return

    # Handles updating Rolled Out material props on Blender prop update
    if wrapper_attrib != attrib:
        setattr(self, attrib_name, wrapper_attrib)

# Register
def register():
    try:
        for m in modules:
            for name,cls in inspect.getmembers(m, inspect.isclass):
                if hasattr(cls, "bl_rna"):
                    bpy.utils.register_class(cls)
                    print("Registered: " + name)

    except:
        print("Failed to register: " + name)
        traceback.print_exc()

    if on_load_file not in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(on_load_file)

    if on_pre_frame_render not in bpy.app.handlers.depsgraph_update_pre:
        bpy.app.handlers.depsgraph_update_pre.append(on_pre_frame_render)

    bpy.types.Scene.draw_objects = bpy.props.BoolProperty(
        name="Stage Object Visibility",
        default=True)
    bpy.types.Scene.quat_bake_timestep = bpy.props.IntProperty(
        name="Quat Bake Timestep",
        default=2)
    bpy.types.Scene.stage_timer_seconds = bpy.props.IntProperty(
        name="Stage Timer (s)",
        default=60)
    bpy.types.Scene.texture_path_override = bpy.props.EnumProperty(
        name="Texture Path Type",
        description="Specifies which path in which the stage will look for textures in.",
        default="relative",
        items={("relative","Relative",""),
            ("tex","Stages/tex Folder",""),
            ("stage","Stage Folder",""),
            }
        )

    bpy.types.Object.mesh_node_properties = bpy.props.PointerProperty(type=stage_edit_mode_operator.MeshNodeProperties)
    bpy.types.Object.wormhole_node_properties = bpy.props.PointerProperty(type=stage_edit_mode_operator.WormholeNodeProperties)
    bpy.types.Object.switch_node_properties = bpy.props.PointerProperty(type=stage_edit_mode_operator.SwitchNodeProperties)
    bpy.types.Object.animation_id = bpy.props.IntProperty(
        name="Animation ID",
        default=-1)
    bpy.types.Object.animation_init_timescale = bpy.props.FloatProperty(
        name="Animation Init. Timescale",
        default=1.0)
    bpy.types.Object.animation_loop = bpy.props.BoolProperty(
        name="Animation Loop",
        default=True)
    bpy.types.Material.animation_loop = bpy.props.BoolProperty(
        name="Animation Loop",
        default=True)
    bpy.types.Material.ro_material_type = bpy.props.EnumProperty(
        name="Material Type",
        description="",
        items=[('rolledout:mat_surface_opaque_lit_generic' ,'Fully Opaque (lit)',''),
               ('rolledout:mat_surface_translucent_lit_generic' ,'Transluscent (lit)',''),
               ('rolledout:mat_surface_masked_lit_generic' ,'Alpha Masked (lit)',''),
               ('rolledout:mat_surface_opaque_unlit_generic' ,'Fully Opaque (unlit)',''),
               ('rolledout:mat_surface_translucent_unlit_generic' ,'Transluscent (unlit)',''),
               ('rolledout:mat_surface_opaque_unlit_generic' ,'Alpha Masked (unlit)',''),
               ('rolledout:mat_surface_additive_generic' ,'Additive',''),
               ('rolledout:mat_surface_overlay_generic' ,'Overlay',''),
               ('rolledout:mat_surface_multiply_generic' ,'Multiply',''),
               ('rolledout:mat_surface_default'  ,'Surface Default',''),
               ('rolledout:mat_surface_gravity_surface' ,'Gravity Surface',''),
               ('rolledout:mat_surface_ice_surface' ,'Ice Surface',''),
               ('rolledout:mat_surface_invisible' ,'Invisible',''),
               ('rolledout:mat_surface_sticky_surface' ,'Sticky Surface','')],
        )
    bpy.types.Material.ro_attach_to_touch_platform_group = bpy.props.BoolProperty(
        name="Attach to touch platform group?",
        description="",
        default=False)
    bpy.types.Material.ro_touch_platform_group_id = bpy.props.IntProperty(
        name="Touch platform group ID",
        description="",
        default=0)

    bpy.types.Material.base_color_multiplier = bpy.props.FloatVectorProperty(
            name="Base Color Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=(1,1,1),
            subtype='COLOR')
            #update=lambda s,c: update_mat(s, c, "base_color_multiplier"))
    bpy.types.Material.emissive_multiplier = bpy.props.FloatVectorProperty(
            name="Emissive Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=(0,0,0),
            subtype='COLOR')
            #  update=lambda s,c: update_mat(s, c, "emissive_multiplier"))
    bpy.types.Material.metallic_multiplier = bpy.props.FloatProperty(
            name="Metallic Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=0.0,
            update=lambda s,c: update_mat(s, c, "metallic_multiplier"),
            soft_min=0.0,
            soft_max=1.0)
    bpy.types.Material.specular_multiplier = bpy.props.FloatProperty(
            name="Specular Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=0.0,
            update=lambda s,c: update_mat(s, c, "specular_multiplier"),
            soft_min=0.0,
            soft_max=1.0)
    bpy.types.Material.roughness_multiplier = bpy.props.FloatProperty(
            name="Roughness Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=0.5,
            update=lambda s,c: update_mat(s, c, "roughness_multiplier"),
            soft_min=0.0,
            soft_max=1.0)
    bpy.types.Material.refraction_multiplier = bpy.props.FloatProperty(
            name="Refraction Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=1.450,
            update=lambda s,c: update_mat(s, c, "refraction_multiplier"),
            soft_min=0.0)
    bpy.types.Material.opacity_multiplier = bpy.props.FloatProperty(
            name="Opacity Multiplier",
            description="The vector multiplier used for the overall color of the material.",
            default=1.0,
            update=lambda s,c: update_mat(s, c, "opacity_multiplier"),
            soft_min=0.0,
            soft_max=1.0)
    bpy.types.Material.tex_coord_multiplier = bpy.props.FloatVectorProperty(
            name="Texture Scale",
            description="The vector multiplier used to scale all textures in the material.",
            default=(1,1),
            size=2)
    bpy.types.Material.tex_coord_offset = bpy.props.FloatVectorProperty(
            name="Texture Offset",
            description="The vector multiplier used to translate all textures in the material.",
            default=(1,1),
            size=2)

    menus.handle_register()

    print("Registered {} with {} modules".format(bl_info["name"], len(modules)))
    for module in modules:
        print("-", module.__name__)

def unregister():
    menus.handle_unregister()

    del bpy.types.Scene.quat_bake_timestep
    del bpy.types.Scene.draw_objects
    del bpy.types.Scene.stage_timer_seconds
    del bpy.types.Object.mesh_node_properties
    del bpy.types.Object.wormhole_node_properties
    del bpy.types.Object.switch_node_properties
    del bpy.types.Object.animation_id

    try:
        # Remove draw handlers
        for handler in statics.active_draw_handlers:
            bpy.types.SpaceView3D.draw_handler_remove(handler, 'WINDOW')

        for m in modules:
            for name,cls in inspect.getmembers(m, inspect.isclass):
                if hasattr(cls, "bl_rna"):
                    bpy.utils.unregister_class(cls)
                    print("Unregistered: " + name)
    except:
        traceback.print_exc()

    if on_load_file in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(on_load_file)

    if on_pre_frame_render in bpy.app.handlers.depsgraph_update_pre:
        bpy.app.handlers.depsgraph_update_pre.remove(on_pre_frame_render)

    print("Unregistered {}".format(bl_info["name"]))

