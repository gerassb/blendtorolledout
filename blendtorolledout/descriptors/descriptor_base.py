class DescriptorBase:
    @staticmethod
    def get_object_name():
        raise NotImplementedError

    @staticmethod
    def get_node_type():
        raise NotImplementedError

    @staticmethod
    def get_node_properties(obj):
        pass

    @staticmethod
    def render(obj):
        raise NotImplementedError
