import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorMine(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[MINE]"

    @staticmethod
    def get_node_type():
        return "mine_node"

    @staticmethod
    def render(obj):
        obj_drawing.get_draw_misc_col((0.96, 0.26, 0.21))(obj)
