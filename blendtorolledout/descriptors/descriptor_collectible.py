import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorCollectible1(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[COLLECTIBLE_1]"

    @staticmethod
    def get_node_type():
        return "collectable_node"

    @staticmethod
    def render(obj):
        obj_drawing.get_draw_misc_col((0.13, 0.59, 0.95))(obj)

class DescriptorCollectible5(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[COLLECTIBLE_5]"

    @staticmethod
    def get_node_type():
        return "collectable_node"

    @staticmethod
    def render(obj):
        obj_drawing.get_draw_misc_col((0.18, 0.83, 0.11))(obj)

class DescriptorCollectible10(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[COLLECTIBLE_10]"

    @staticmethod
    def get_node_type():
        return "collectable_node"

    @staticmethod
    def render(obj):
        obj_drawing.get_draw_misc_col((0.96, 0.26, 0.21))(obj)
