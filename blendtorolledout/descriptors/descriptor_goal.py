import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorGoalB(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[GOAL_B]"

    @staticmethod
    def get_node_type():
        return "goal_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_goal(obj, (0.13, 0.59, 0.95))


class DescriptorGoalG(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[GOAL_G]"

    @staticmethod
    def get_node_type():
        return "goal_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_goal(obj, (0.30, 0.69, 0.31))


class DescriptorGoalR(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[GOAL_R]"

    @staticmethod
    def get_node_type():
        return "goal_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_goal(obj, (0.96, 0.26, 0.21))
