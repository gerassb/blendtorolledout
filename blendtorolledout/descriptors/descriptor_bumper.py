import sys

from .. import math_utils
from .. import obj_drawing
from .descriptor_base import DescriptorBase

import xml.etree.ElementTree as etree


class DescriptorBumper(DescriptorBase):
    @staticmethod
    def get_object_name():
        return "[BUMPER]"

    @staticmethod
    def get_node_type():
        return "bumper_node"

    @staticmethod
    def render(obj):
        obj_drawing.draw_bumper(obj)
