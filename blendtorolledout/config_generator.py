import sys
import json
import re

import bpy
import bpy_extras
import mathutils
import pdb

from . import material_properties
from mathutils import Quaternion, Vector
from bpy_extras import node_shader_utils
from .stage_data import StageData
from .descriptors import descriptors
from .result import Result, Ok, Err


def generate_config(stage_data: StageData, fbx_name: str, progress_callback=None):
    # These globals should be defined in the stage metadata script
    global stage_metadata
    global stage_background_map

    print("Generating config!")

    config_obj = {
        "config_version": "0.3.0",
        "model_files": [
            {
                "name": "model_main",
                "file_path": fbx_name,
            },
        ],
    }

    # First fetch metadata
    # The metadata can basically be copied one-for-one
    textblock = bpy.data.texts.get("blendtorolledout:metadata")

    meta_script = textblock.as_string()
    # Certainly not the prettiest but it'll do!
    exec(meta_script)  # Should set a global "stage_metadata" and "stage_background_map"
    config_obj["metadata"] = stage_metadata
    config_obj["timer_seconds"] = bpy.context.scene.stage_timer_seconds
    config_obj["background_map"] = stage_background_map

    # TODO: scripts

    # Find spawn point
    for obj in bpy.context.scene.objects:
        if "[START]" in obj.name:
            if "spawns" in config_obj:
                return Err("Multiple spawns in the scene")

            obj_pos = obj.matrix_world.to_translation()
            obj_rot = obj.matrix_world.to_quaternion()
            # The visual indicator for spawn points is actually incorrect, this fixes it
            obj_rot = obj_rot @ Quaternion((0.70710, 0, 0, -0.70710))

            config_obj["spawns"] = [
                {
                    "name": "spawn_main",
                    "transform_noscale": {
                        "position": get_position_json(obj_pos),
                        "rotation": get_rotation_json(obj_rot),
                    },
                    "gravity_vector": { "x": 0.0, "y": 0.0, "z": -1.0 },
                },
            ]

    # Add materials
    materials = []
    for mat in bpy.data.materials:
        if mat.users > 0 and not mat.is_grease_pencil:

            mat_type = mat.ro_material_type
            config_mat = {
                "name": sanitize_name(mat.name),
                "base_material": mat_type
            }

            mat_wrap = node_shader_utils.PrincipledBSDFWrapper(mat)

            config_mat["animations"] = []

            if mat.animation_data is not None and mat.animation_data.action is not None:
                config_mat["animations"] = [
                    sanitize_name(mat.animation_data.action.name),
                ]

            config_mat["parameter_overrides"] = {}

            for attrib in material_properties.mat_props_ro_material[mat_type] + material_properties.mat_props_ro_wrapper_exclusive[mat_type]:

                #print("new attrib: " + str(attrib))
                if attrib in material_properties.mat_props_wrapper_attribs.keys():
                    attrib_type = material_properties.mat_props_wrapper_attribs[attrib]
                    attrib_value = getattr(mat_wrap, attrib_type)
                #    print("getting attrib from wrapper: " + str(attrib))
                else:
                    attrib_value = getattr(mat, attrib)
                #    print("getting attrib from mat: " + str(attrib))

                if attrib_value is not None:
                    if isinstance(attrib_value, node_shader_utils.ShaderImageTextureWrapper):
                        if attrib_value.image is not None:
                            config_mat["parameter_overrides"][attrib] = {}
                            config_mat["parameter_overrides"][attrib]["type"] = "texture2d"
                            config_mat["parameter_overrides"][attrib]["value"] = sanitize_name(attrib_value.image.name)
                        continue

                    elif hasattr(attrib_value, '__getitem__'):
                        if len(attrib_value) == 2:
                            config_mat["parameter_overrides"][attrib] = {}
                            config_mat["parameter_overrides"][attrib]["type"] = "vector2"
                            config_mat["parameter_overrides"][attrib]["value"] = get_vector2_json(attrib_value)

                        elif len(attrib_value) == 3:
                            config_mat["parameter_overrides"][attrib] = {}
                            config_mat["parameter_overrides"][attrib]["type"] = "vector3"
                            config_mat["parameter_overrides"][attrib]["value"] = get_vector_json(attrib_value)
                    
                    else:
                        config_mat["parameter_overrides"][attrib] = {}
                        config_mat["parameter_overrides"][attrib]["type"] = "scalar" 
                        config_mat["parameter_overrides"][attrib]["value"] = attrib_value

            materials.append(config_mat)

    config_obj["materials"] = materials

    # Add textures
    textures = []
    for mat in bpy.data.materials:
        if mat.users > 0 and not mat.is_grease_pencil:
            for link in mat.node_tree.links:
                if link.from_node.bl_idname == "ShaderNodeTexImage":
                    tex = link.from_node
                    if None not in [tex, tex.image]:
                        # Don't add a texture if it's already added
                        texture_exists = False
                        for existing_texture in textures:
                            if existing_texture["name"] == sanitize_name(tex.image.name):
                                texture_exists = True
                                break

                        if texture_exists: continue
                            
                        path_type = bpy.context.scene.texture_path_override
                        
                        if path_type == "relative":
                            file_path = "//../" + tex.image.filepath[2:].replace("\\", "/")
                        elif path_type == "tex":
                            file_path = "//../../tex/" + bpy.path.basename(tex.image.filepath)
                        elif path_type == "stage":
                            file_path = "//" + bpy.path.basename(tex.image.filepath)
                        textures.append({
                            "name": sanitize_name(tex.image.name),
                            "file_path": file_path,
                        })

    config_obj["texture_files"] = textures

    # Add curves and animations
    actions = [obj for obj in bpy.context.scene.objects if (obj.animation_data is not None and obj.animation_data.action is not None)]

    # Add material animations
   
    actions.extend([mat for mat in bpy.data.materials if mat.animation_data is not None and mat.animation_data.action is not None])

    animations = []
    float_curves = []
    quat_curves = []

    for obj in actions:
        action = obj.animation_data.action
        anim_name = get_anim_name_for_obj(obj)

        # Float curves/keyframes {{{

        float_channels = {}
        for i, curve in enumerate(action.fcurves):
            x_scale = 1 / bpy.context.scene.render.fps
            y_scale = 1

            curve_name = anim_name + "_float_curve" + str(i)
            if isinstance(obj, bpy.types.Object):
                channel_data_path = "transform."
            else:
                channel_data_path = "parameter_overrides."

            if curve.data_path == "location":
                y_scale = 100
                channel_data_path += "position."
                if curve.array_index == 0:
                    channel_data_path += "x"
                    y_scale *= -1
                elif curve.array_index == 1:
                    channel_data_path += "y"
                elif curve.array_index == 2:
                    channel_data_path += "z"
                else:
                    return Err("Got a location f-curve that has an array index not in range 0-2")

            elif curve.data_path == "scale":
                channel_data_path += "scale."
                if curve.array_index == 0:
                    channel_data_path += "x"
                elif curve.array_index == 1:
                    channel_data_path += "y"
                elif curve.array_index == 2:
                    channel_data_path += "z"

            #  elif curve.data_path == "rotation_quaternion":
                #  # HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALLLLLLLLLPPPPPPPPPPPPPP
                #  # Screw rotations
                #  # This is is pure freakin' magic, I tell ya. I'm not a mathemagician, despite what that one pencil says
                #  # that I got from a math teacher in secondary school.
                #  channel_data_path += "rotation."
                #  if curve.array_index == 0:
                    #  channel_data_path += "w"
                    #  y_scale *= -1
                #  elif curve.array_index == 1:
                    #  channel_data_path += "x"
                    #  y_scale *= -1
                #  elif curve.array_index == 2:
                    #  channel_data_path += "y"
                #  elif curve.array_index == 3:
                    #  channel_data_path += "z"
                #  else:
                    #  return Err("Got a rotation quat f-curve that has an array index not in range 0-3")

            elif curve.data_path in material_properties.mat_scalar_prop_name_list:
                channel_data_path += "scalar."
                channel_data_path += curve.data_path
            elif curve.data_path in material_properties.mat_vector2_prop_name_list:
                channel_data_path += "vector2."
                channel_data_path += curve.data_path
                if curve.array_index == 0:
                    channel_data_path += ".x"
                elif curve.array_index == 1:
                    channel_data_path += ".y"
            elif curve.data_path in material_properties.mat_vector3_prop_name_list:
                channel_data_path += "vector3."
                channel_data_path += curve.data_path
                if curve.array_index == 0:
                    channel_data_path += ".x"
                elif curve.array_index == 1:
                    channel_data_path += ".y"
                elif curve.array_index == 2:
                    channel_data_path += ".z"

            else:
                continue

            keyframes = []
            for kf in curve.keyframe_points:
                # Used to provide an error describing what keyframe we have an issue with if stuff goes wrong
                def describe_keyframe():
                    return ("In object: %s\n" % obj.name) + \
                        ("In action: %s\n" % action.name) + \
                        ("In curve: %s[%d]\n" % (curve.data_path, curve.array_index)) + \
                        ("In keyframe at: (%f, %f)" % (kf.co.x, kf.co.y))

                control_point_x_value = kf.co.x * x_scale
                control_point_y_value = kf.co.y * y_scale
                left_handle_x_value = kf.handle_left.x * x_scale
                left_handle_y_value = kf.handle_left.y * y_scale
                right_handle_x_value = kf.handle_right.x * x_scale
                right_handle_y_value = kf.handle_right.y * y_scale

                # If the object has a parent, keyframes need to relative to the parent rather than glboal
                if (curve.data_path == "location" and obj.parent is not None):
                    print(obj.name + "with parent" + obj.parent.name + "keyframes being adjusted")
                    local_transform = obj.matrix_local.to_translation()
                    origin_transform_value = curve.evaluate(bpy.context.scene.frame_current)
                    transform_delta = (kf.co.y - origin_transform_value)
                    local_keyframe_value = local_transform[curve.array_index] + transform_delta
                    local_keyframe_delta = local_keyframe_value - kf.co.y

                    control_point_y_value = local_keyframe_value * y_scale 
                    left_handle_y_value = (kf.handle_left.y + local_keyframe_delta) * y_scale
                    right_handle_y_value = (kf.handle_right.y + local_keyframe_delta) * y_scale


                # Round the handle value (potential fix for weird stuttering issues..?)
                left_handle_x_value = round(left_handle_x_value, 5)
                left_handle_y_value = round(left_handle_y_value, 5)
                right_handle_x_value = round(right_handle_x_value, 5)
                right_handle_y_value = round(right_handle_y_value, 5)

                # Convert the interpolation type to a Rolled Out! one
                if kf.interpolation == "CONSTANT":
                    interp_type = "constant"
                elif kf.interpolation == "LINEAR":
                    interp_type = "linear"
                elif kf.interpolation == "BEZIER":
                    interp_type = "cubic_bezier"
                else:
                    return Err("Unsupported keyframe interpolation type " + kf.interpolation + "\n" + describe_keyframe())

                # Convert the left handle type to a Rolled Out! one
                if kf.handle_left_type == "AUTO_CLAMPED":
                    handle_left_type = "auto_clamped"
                elif kf.handle_left_type == "FREE":
                    handle_left_type = "free"
                elif kf.handle_left_type == "VECTOR":
                    handle_left_type = "vector"
                else:
                    return Err("Unsupported left handle type " + kf.handle_left_type + "\n" + describe_keyframe())

                # Convert the right handle type to a Rolled Out! one
                if kf.handle_right_type == "AUTO_CLAMPED":
                    handle_right_type = "auto_clamped"
                elif kf.handle_right_type == "FREE":
                    handle_right_type = "free"
                elif kf.handle_right_type == "VECTOR":
                    handle_right_type = "vector"
                else:
                    return Err("Unsupported right handle type " + kf.handle_right_type + "\n" + describe_keyframe())

                keyframes.append({
                    "interpolation": {
                        "type": interp_type,
                    },
                    "left_handle": {
                        "position": { "x": left_handle_x_value, "y": left_handle_y_value },
                        "type": handle_left_type,
                    },
                    "control_point": {
                        "position": { "x": control_point_x_value, "y": control_point_y_value },
                    },
                    "right_handle": {
                        "position": { "x": right_handle_x_value, "y": right_handle_y_value },
                        "type": handle_right_type,
                    },
                })
            float_curves.append({
                "name": curve_name,
                "keyframes": keyframes,
            })

            float_channels[channel_data_path] = {
                "float_curve": curve_name,
                "transform_curve": {
                    "position": { "x": 0.0, "y": 0.0 },
                    "scale": { "x": 1.0, "y": 1.0 }
                }
            }

        # }}}

        # Quat curves/keyframes {{{

        # HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALLLLLLLLLPPPPPPPPPPPPPP
        # Screw rotations
        # This is is pure freakin' magic, I tell ya. I'm not a mathemagician, despite what that one pencil says
        # that I got from a math teacher in secondary school.
        quat_channels = {}
        for i, curve in enumerate(action.fcurves):
            x_scale = 1 / bpy.context.scene.render.fps

            curve_name = anim_name + "_quat_curve" + str(i)
            channel_data_path = "transform.rotation"

            # We're just going off of the W keyframes...
            if not (curve.data_path == "rotation_quaternion" and curve.array_index == 0):
                continue

            keyframes = []
            for kf in curve.keyframe_points:
                rot_w = kf.co.y
                rot_x = action.fcurves.find("rotation_quaternion", index=1).evaluate(kf.co.x)
                rot_y = action.fcurves.find("rotation_quaternion", index=2).evaluate(kf.co.x)
                rot_z = action.fcurves.find("rotation_quaternion", index=3).evaluate(kf.co.x)

                # Handles the rotation of parented objects
                if obj.parent is not None and obj.parent.type != "EMPTY":
                    global_rotation = obj.matrix_world.to_quaternion()
                    local_rotation = obj.matrix_local.to_quaternion()
                    keyframe_rotation = Quaternion((rot_w, rot_x, rot_y, rot_z))

                    final_rotation = global_rotation.rotation_difference(keyframe_rotation)

                    print(f"O: {obj.name} F: {kf.co.x}\n\tglobal: {global_rotation}\n\tlocal: {local_rotation}\n\tkeyframe: {keyframe_rotation})\n\tfinal: {final_rotation}")

                    rot_w = final_rotation[0]
                    rot_x = final_rotation[1]
                    rot_y = final_rotation[2]
                    rot_z = final_rotation[3]

                # Convert the interpolation type to a Rolled Out! one
                if kf.interpolation == "CONSTANT":
                    interp_type = "constant"
                elif kf.interpolation == "LINEAR":
                    interp_type = "linear"
                elif kf.interpolation == "BEZIER":
                    interp_type = "cubic_bezier"
                else:
                    return Err("Unsupported keyframe interpolation type " + kf.interpolation)

                # Convert the left handle type to a Rolled Out! one
                if kf.handle_left_type == "AUTO_CLAMPED":
                    handle_left_type = "auto_clamped"
                elif kf.handle_left_type == "FREE":
                    handle_left_type = "free"
                elif kf.handle_left_type == "VECTOR":
                    handle_left_type = "vector"
                else:
                    return Err("Unsupported left handle type " + kf.handle_left_type)

                # Convert the right handle type to a Rolled Out! one
                if kf.handle_right_type == "AUTO_CLAMPED":
                    handle_right_type = "auto_clamped"
                elif kf.handle_right_type == "FREE":
                    handle_right_type = "free"
                elif kf.handle_right_type == "VECTOR":
                    handle_right_type = "vector"
                else:
                    return Err("Unsupported right handle type " + kf.handle_right_type)

                # Ok so... trying to figure out how interpolation handles should work here is tricky... so we just do
                # some simple heuristics instead because I don't feel like figuring out how to do it the-proper-way

                keyframes.append({
                    "interpolation": {
                        #  "type": interp_type,
                        "type": "linear",  # TODO!!!
                    },
                    "left_handle": {
                        "position": { "x": kf.handle_left.x * x_scale, "y": 1.0 },
                        "type": handle_left_type,
                    },
                    "control_point": {
                        "position": {"x": kf.co.x * x_scale, "y": {
                            "x": rot_x * -1,
                            "y": rot_y,
                            "z": rot_z,
                            "w": rot_w * -1,
                        }},
                    },
                    "right_handle": {
                        "position": { "x": kf.handle_right.x * x_scale, "y": 1.0 },
                        "type": handle_right_type,
                    },
                })

            quat_curves.append({
                "name": curve_name,
                "keyframes": keyframes,
            })

            quat_channels[channel_data_path] = {
                "quaternion_curve": curve_name,
                "transform_curve": {
                    "position": { "x": 0.0, "y": 0.0 },
                    "scale": { "x": 1.0, "y": 1.0 }
                }
            }

        # }}}

        new_anim = {
            "name": anim_name,
            "initial_time_scale": obj.get("animation_init_timescale", 1.0),
            "float_channels": float_channels,
            "quaternion_channels": quat_channels,
        }

        if obj.animation_loop:
            new_anim["loop_zone"] = get_anim_loop_zone()

        animations.append(new_anim)

    config_obj["float_curves"] = float_curves
    config_obj["quaternion_curves"] = quat_curves
    config_obj["animations"] = animations

    # TODO: We should get rid of this eventually (barring a "ping_pong" toggle) as whether a touch platform loops or not
    #       is set by if the animations it's attached to loops or not
    playback_type_lookup = {
            "0": "play_once",
            "1": "loop",
            "2": "ping_pong",
            }

    tplatforms = []
    if "touch_platform_groups" in bpy.context.scene.keys():
        tplatforms_list = bpy.context.scene["touch_platform_groups"]
        for group_id in tplatforms_list.keys():
            playback_type = playback_type_lookup[str(tplatforms_list[group_id])]

            config_group = {
                #  "playback_type": playback_type_lookup[str(tplatforms_list[group_id])],
                "name": get_touch_platform_group_name_for_id(group_id),
                "ping_pong": playback_type == "ping_pong",
                "playback_speed": 1.0,
            }
            anims = []

            # Add objects to the anim list
            for obj in bpy.context.scene.objects:
                if obj.get("ro:bool:touchPlatform", False) and ("ro:int:touchPlatformGroup" in obj.keys() and obj["ro:int:touchPlatformGroup"] == int(group_id)):
                    anims.append(get_anim_name_for_obj(obj))

                    # We also need to fix up animation loops/start speeds here, for *compatability* or something
                    print("Fixing up touch platform animations for " + obj.name)

                    anim_name = get_anim_name_for_obj(obj)
                    anim = next(anim for anim in config_obj["animations"] if anim["name"] == anim_name)
                    anim["initial_time_scale"] = 0.0
                    if playback_type == "play_once" or playback_type == "ping_pong":
                        # Remove the animation loop zone if it has one
                        anim.pop("loop_zone", None)
                    else:  # Looping
                        # Add a loop zone
                        anim["loop_zone"] = get_anim_loop_zone()

            # Add materials to the anim list
            for mat in bpy.data.materials:
                if mat.get("ro_attach_to_touch_platform_group", False) and mat.get("ro_touch_platform_group_id", 0) == int(group_id):
                    anims.append(get_anim_name_for_obj(mat))

                    # We also need to fix up animation loops/start speeds here
                    print("Fixing up touch platform animations for " + mat.name)

                    anim_name = get_anim_name_for_obj(mat)
                    anim = next(anim for anim in config_obj["animations"] if anim["name"] == anim_name)
                    anim["initial_time_scale"] = 0.0
                    if playback_type == "play_once" or playback_type == "ping_pong":
                        # Remove the animation loop zone if it has one
                        anim.pop("loop_zone", None)
                    else:  # Looping
                        # Add a loop zone
                        anim["loop_zone"] = get_anim_loop_zone()

            config_group["animations"] = anims

            if len(anims) > 0:
                tplatforms.append(config_group)

    config_obj["touch_platform_groups"] = tplatforms

    # Add the scene graph
    def recurse_get_scene_graph(parent):
        children = []

        for obj in get_obj_children(parent):
            if "[START]" in obj.name:
                continue

            node_type = None
            for desc in descriptors.descriptors:
                if desc.get_object_name() in obj.name:
                    node_type = desc.get_node_type()

            # Some horrible special cases
            if node_type is None:
                if obj.type == "EMPTY":
                    node_type = "scene_node"
                elif obj.type == "MESH":
                    if len(obj.data.vertices) > 0:
                        node_type = "mesh_node"
                    else:
                        node_type = "scene_node"
                else:
                    continue

            node = {
                "name": sanitize_name(obj.name),
                "type": node_type,
                "transform": {
                    "position": get_position_json(obj.matrix_local.to_translation()),
                    "rotation": get_rotation_json(obj.matrix_local.to_quaternion()),
                    "scale": get_scale_json(obj.matrix_local.to_scale()),
                },
                "children": recurse_get_scene_graph(obj),
            }

            if obj.animation_data is not None and obj.animation_data.action is not None:
                node["animations"] = [
                    sanitize_name(obj.animation_data.action.name),
                ]

            data_gen = None
            try:
                data_gen = node_data_generators[node_type]
            except KeyError:
                # There is no generator for this node type
                pass

            if data_gen is not None:
                node_data = data_gen(obj)
                node["node_data"] = node_data

            children.append(node)

        return children

    config_obj["scene_graph"] = recurse_get_scene_graph(None)

    config_str = json.dumps(config_obj, separators=(",", ":"))
    return Ok(config_str)


def get_obj_children(parent):
    children = []
    for obj in bpy.context.scene.objects:
        if obj.parent == parent:
            children.append(obj)

    return children


def sanitize_name(name: str):
    return re.sub("[^0-9a-z_]", "_", name.lower())


def gen_mesh_node_data(obj):
    data = {
        "mesh_reference": {
            "mesh_file_name": "model_main",
            "mesh_object_name": get_obj_name(obj),
        },
        "gravity_surface": bool(obj.get("ro:bool:gravitySurface", False)),
        "ice_surface": bool(obj.get("ro:bool:iceSurface", False)),
        "seesaw_surface": bool(obj.get("ro:bool:seesawSurface", False)),
        "sticky_surface": bool(obj.get("ro:bool:stickySurface", False)),
        "rotation_axis": str(obj.get("ro:string:rotationAxis", "none")),
        # Divided by 10,000 as Blender rounds the default value to 0.00001
        "seesaw_sensitivity": float(obj.get("ro:float:seesawSensitivity", 0.0)/10000.0),
        "seesaw_springiness": float(obj.get("ro:float:seesawSpringiness", 0.0)),
        "seesaw_friction": float(obj.get("ro:float:seesawFriction", 0.0)),
        "spring": bool(obj.get("ro:bool:springSurface", False)),
        "spring_impulse": get_vector_json(obj.get("ro:vector:springSurfaceImpulse", Vector((0,0,0)))),
        "gravity_line_trace_enabled": bool(obj.get("ro:bool:gravitySurfaceLineTrace", True)),
        "fallout_volume": bool(obj.get("ro:bool:falloutVolume", False)),
        "conveyor": bool(obj.get("ro:bool:conveyorSurface", False)),
        "conveyor_local_velocity": get_vector_json(obj.get("ro:vector:conveyorSurfaceVelocity", Vector((0,0,0)))),
        "conveyor_uv_velocity": get_vector2_json(obj.get("ro:vector:conveyorSurfaceUVVelocity", Vector((0,0)))), 
        "wind_volume": bool(obj.get("ro:bool:windVolume", False)),
        "wind_velocity": get_vector_json(obj.get("ro:vector:windVolumeVelocity", Vector((0,0,0)))),
        "collision_enabled": bool(obj.get("ro:bool:collisionEnabled", True)),
        "quicksand_surface": bool(obj.get("ro:bool:quicksandSurface", False)),
    }

    if bool(obj.get("ro:bool:touchPlatform", False) and obj.get("ro:bool:touchPlatformTrigger", True)) and "ro:int:touchPlatformGroup" in obj:
        data["activate_touch_platform_groups"] = [
            get_touch_platform_group_name_for_id(obj["ro:int:touchPlatformGroup"])
        ]

    # Append materials in the correct order
    mat_slots = []
    mesh = obj.to_mesh()
    last_index = None
    sort_func = lambda a: (a.material_index)
    mesh_faces = list(mesh.polygons)
    mesh_faces.sort(key=sort_func)

    if mesh_faces is not None:
        for face in mesh_faces:
            mat_index = face.material_index
            if (mat_index != None) and (mat_index != last_index):
                if len(obj.data.materials) == 0:
                    raise Exception(f"Mesh {obj.name} has no materials associated with it")
                last_index = face.material_index
                mat_slots.append(sanitize_name(obj.data.materials[mat_index].name))

    data["material_slots"] = mat_slots

    return data

def gen_goal_node_data(obj):
    warp_distance = 1
    if "[GOAL_G]" in obj.name:
        warp_distance = 2
    elif "[GOAL_R]" in obj.name:
        warp_distance = 3

    data = {
        "warp_distance": warp_distance
    }

    return data

def gen_wormhole_node_data(obj):
    obj_linked = obj.get("ro:node:linkedNode", None)
    if obj_linked is not None: obj_name = obj_linked.name
    else: obj_name = "none"

    data = {
        "link": sanitize_name(obj_name),
        "gravity_align_self": bool(obj.get("ro:bool:gravityAlignSelf", False)),
        "gravity_align_link": bool(obj.get("ro:bool:gravityAlignLinked", False)),   
    }
    return data

def gen_switch_node_data(obj):
    # Every node has an animation ID associated with it, -1 by default
    # If a switch's linked ID matches an object's animation ID, the object is added to the list
    linkedNodes = []
    switch_id = obj.switch_node_properties.linkedNodeId
    for node in bpy.data.objects:
        if node.animation_id == switch_id:
            linkedNodes.append(sanitize_name(node.name))

    data = {
        "initially_pressed": bool(obj.get("ro:bool:initiallyPressed", False)),
        "playback_speed": float(obj.get("ro:float:playbackSpeed", 0.0)),
        "link_nodes": linkedNodes,
        "button_visual": str(obj.get("ro:string:buttonVisual", "none"))
    }
    return data

def gen_collectable_node_data(obj):
    if "1]" in obj.name:
        amount = 1
    elif "5]" in obj.name:
        amount = 5
    elif "10]" in obj.name:
        amount = 10
    else:
        amount = 1

    data = {
        "amount": amount
    }

    return data

def get_position_json(pos):
    return {
        "x": pos.x * -100,
        "y": pos.y * 100,
        "z": pos.z * 100,
    }

def get_rotation_json(rot):
    return {
        "x": -rot.x,
        "y": rot.y,
        "z": rot.z,
        "w": -rot.w,
    }

def get_scale_json(scl):
    return {
        "x": scl.x,
        "y": scl.y,
        "z": scl.z,
    }

def get_vector_json(vec):
    return {
        "x": vec[0],
        "y": vec[1],
        "z": vec[2],
    }

def get_vector2_json(vec):
    return {
        "x": vec[0],
        "y": vec[1],
    }

def get_obj_name(obj):
    if obj.name == obj.data.name:
        return obj.name.replace(" ", "_")
    else:
        return (obj.name + "_" + obj.data.name).replace(" ", "_")

def get_anim_name_for_obj(obj):
    if obj.animation_data is not None and obj.animation_data.action is not None:
        return sanitize_name(obj.animation_data.action.name)
    else:
        raise KeyError("Object '" + obj.name + "' does not have an animation action")

def get_touch_platform_group_name_for_id(group_id):
    return "touch_platform_group" + str(group_id)

def get_anim_loop_zone():
    return {
        # TODO: I think this causes some hitches around the loop point - figure out why
        "start_time": (bpy.context.scene.frame_start - 1) / bpy.context.scene.render.fps,
        "end_time": bpy.context.scene.frame_end / bpy.context.scene.render.fps,
    }

# TODO: More generators for goals, bumpers, etc.
node_data_generators = {
    "mesh_node": gen_mesh_node_data,
    "goal_node": gen_goal_node_data,
    "wormhole_node": gen_wormhole_node_data,
    "switch_node": gen_switch_node_data,
    "collectable_node": gen_collectable_node_data,
}
