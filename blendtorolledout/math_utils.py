def get_object_rot_quat(obj):
    if "Z" in obj.rotation_mode:
        return obj.rotation_euler.to_quaternion()
    else:
        return obj.rotation_quaternion
