import math
import mathutils
import gpu
import bgl

from gpu_extras.batch import batch_for_shader
from mathutils import Matrix, Vector

COLOR_BLACK = (0.0, 0.0, 0.0, 0.8)
COLOR_BLUE = (0.13, 0.59, 0.95, 0.8)
COLOR_RED = (0.96, 0.26, 0.21, 0.8)
COLOR_GREEN = (0.18, 0.83, 0.11, 0.8)
COLOR_YELLOW = (1.0, 0.92, 0.23, 0.8)
COLOR_PURPLE = (0.40, 0.23, 0.72, 0.8)

def norm(vec):
    return [float(i) / sum(vec) for i in vec]

def draw_batch(coord, color, primitive_type):
    shader = gpu.shader.from_builtin("3D_UNIFORM_COLOR")
    batch = batch_for_shader(shader, primitive_type, {"pos": coord})
    shader.bind()
    shader.uniform_float("color", color)
    batch.draw(shader)

def draw_grid(start_x, start_y, space_x, space_y, repeat_x, repeat_y, z):
    bgl.glBegin(bgl.GL_LINES)

    for i in range(0, repeat_x + 1):
        bgl.glVertex3f(start_x + space_x * i, start_y, z)
        bgl.glVertex3f(start_x + space_x * i, start_y + space_y * repeat_y, z)

    for i in range(0, repeat_y + 1):
        bgl.glVertex3f(start_x, start_y + space_y * i, z)
        bgl.glVertex3f(start_x + space_x * repeat_x, start_y + space_y * i, z)

    bgl.glEnd()

def rotate_gl(euler_rot):
    x_rot = Matrix.Rotation(euler_rot[0], 4, 'X')
    y_rot = Matrix.Rotation(euler_rot[1], 4, 'Y')
    z_rot = Matrix.Rotation(euler_rot[2], 4, 'Z')
    final_rot = x_rot @ y_rot @ z_rot
    gpu.matrix.multiply_matrix(final_rot)
    
# Draws a circle on the XY plane. 'rot' is a XYZ Vector in degrees
# Probably a smarter way to do this, like drawing it orthagonal to a unit vector
def draw_circle(pos, rot, radius, color, segments, radians=(2*math.pi)):
    gpu.matrix.push()

    coord = []
    for i in range(0, segments+1):
        segment = (i / segments)*radians

        x = pos[0] + radius * math.cos(segment)
        y = pos[1] + radius * math.sin(segment)
        coord.append((x, y, pos[2]))

    rotate_gl(rot)
    draw_batch(coord, color, 'LINE_STRIP')

    gpu.matrix.pop()
    
def draw_sphere(pos, radius, color):
    sphere_rotations = [
            Vector((0,0,0)),
            Vector((90,0,0)),
            Vector((0,90,0)),
    ]

    for rot in sphere_rotations:
        rot_radian = ((math.radians(rot[0]), math.radians(rot[1]), math.radians(rot[2])))
        draw_circle(pos, rot_radian, radius, color, 32)

def draw_box_scaled(pos, scale, color):
    gpu.matrix.push()
    gpu.matrix.translate(pos)
    gpu.matrix.scale(scale)

    coord1 = []

    coord1.append((-0.5, -0.5, -0.5))
    coord1.append((-0.5, -0.5, +0.5))
    coord1.append((-0.5, +0.5, +0.5))
    coord1.append((-0.5, +0.5, -0.5))
    coord1.append((-0.5, -0.5, -0.5))

    coord1.append((+0.5, -0.5, -0.5))
    coord1.append((+0.5, -0.5, +0.5))
    coord1.append((-0.5, -0.5, +0.5))

    coord2 = []
    coord2.append((+0.5, +0.5, +0.5))
    coord2.append((+0.5, +0.5, -0.5))
    coord2.append((+0.5, -0.5, -0.5))
    coord2.append((+0.5, -0.5, +0.5))
    coord2.append((+0.5, +0.5, +0.5))

    coord2.append((-0.5, +0.5, +0.5))
    coord2.append((-0.5, +0.5, -0.5))
    coord2.append((+0.5, +0.5, -0.5))

    draw_batch(coord1, color, "LINE_STRIP")
    draw_batch(coord2, color, "LINE_STRIP")
    gpu.matrix.pop()

# Draw prism or a cone with a base having the number of sides provided by arg 'segments'
def draw_cylinder(pos, rot, radius, height, segments, color, *, cone=False, radians=(2*math.pi)):
    gpu.matrix.push()
    top_origin = Vector((pos[0], pos[1], pos[2]+height))

    draw_circle(pos, rot, radius, color, segments, radians)
    if not cone: draw_circle(top_origin, rot, radius, color, segments, radians)

    coord = []
    for i in range(0, segments+1):
        segment = (i / segments)*radians

        x = pos[0] + radius * math.cos(segment)
        y = pos[1] + radius * math.sin(segment)
        coord.append((x, y, pos[2]))
        if cone:
            coord.append(top_origin)
        else:
            coord.append((x, y, top_origin.z)) 
        
    rotate_gl(rot)
    draw_batch(coord, color, 'LINES')
    
    gpu.matrix.pop()
    
def draw_arrow(start_pos, end_pos, color):
    coord = []

    coord.append(start_pos)
    coord.append(end_pos)

    coord.append(end_pos)
    coord.append((end_pos[0] - 0.2, end_pos[1] - 0.2, end_pos[2]))

    coord.append(end_pos)
    coord.append((end_pos[0] + 0.2, end_pos[1] - 0.2, end_pos[2]))

    draw_batch(coord, color, "LINES")

def draw_double_arrow(start_pos, end_pos, color):
    coord = []

    coord.append(start_pos)
    coord.append((start_pos[0] + 0.2, start_pos[1] + 0.2, start_pos[2]))

    coord.append(start_pos)
    coord.append((start_pos[0] - 0.2, start_pos[1] + 0.2, start_pos[2]))

    coord.append(start_pos)
    coord.append(end_pos)

    coord.append(end_pos)
    coord.append((end_pos[0] - 0.2, end_pos[1] - 0.2, end_pos[2]))

    coord.append(end_pos)
    coord.append((end_pos[0] + 0.2, end_pos[1] - 0.2, end_pos[2]))

    draw_batch(coord, color, "LINES")

def draw_start(obj):
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1 / obj.scale.x, 1 / obj.scale.y, 1 / obj.scale.z))  # No scaling

    bgl.glLineWidth(6)
    draw_sphere((0.0, 0.0, 0.0), 0.5, COLOR_BLACK)
    draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), COLOR_BLACK)

    bgl.glLineWidth(2)
    draw_sphere((0.0, 0.0, 0.0), 0.5, COLOR_BLUE)
    draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), COLOR_BLUE)
    gpu.matrix.pop()

def draw_fallout_volume(obj):
    gpu.matrix.push()
    bgl.glLineWidth(6)
    draw_box_scaled(obj.location, obj.scale, COLOR_BLACK)

    bgl.glLineWidth(2)
    draw_box_scaled(obj.location, obj.scale, COLOR_RED)
    gpu.matrix.pop()

def draw_safe_volume(obj):
    bgl.glLineWidth(6)
    gpu.matrix.push()
    bgl.glLineWidth(6)
    draw_box_scaled(obj.location, obj.scale, COLOR_BLACK)

    bgl.glLineWidth(2)
    draw_box_scaled(obj.location, obj.scale, COLOR_BLUE)
    gpu.matrix.pop()

def draw_goal(obj, color):
    scale = (3, 0.2, 4)
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1 / obj.scale.x, 1 / obj.scale.y, 1 / obj.scale.z))  # No scaling
    gpu.matrix.translate((0, 0, 2))

    bgl.glLineWidth(6)
    draw_box_scaled((0, 0, 0), scale, COLOR_BLACK)
    draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), (*color, 0.8))

    bgl.glLineWidth(2)
    draw_box_scaled((0, 0, 0), scale, (*color, 0.8))
    draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), (*color, 0.8))

    gpu.matrix.pop()

def draw_wh(obj):
    scale = (3, 0.2, 4)
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1 / obj.scale.x, 1 / obj.scale.y, 1 / obj.scale.z))  # No scaling

    lineWidth = [(6, COLOR_BLACK), (2, COLOR_PURPLE)]

    for width, color in lineWidth:
        bgl.glLineWidth(width)
        gpu.matrix.push()
        gpu.matrix.multiply_matrix(Matrix.Rotation(math.radians(90), 4, 'X'))
        draw_circle((0, 1.5, 0), (0, 0, 0), 2, color, 16)
        gpu.matrix.pop()
        gpu.matrix.push()
        gpu.matrix.multiply_matrix(Matrix.Rotation(math.radians(180), 4, 'Z'))
        gpu.matrix.multiply_matrix(Matrix.Translation(Vector((0, -1, 1.5))))
        draw_double_arrow((0,0,0), (0, 1.5, 0), color)
        gpu.matrix.pop()

    gpu.matrix.pop()
    
def draw_fallout(z):
    bgl.glLineWidth(2)
    draw_grid(-512, -512, 4, 4, 256, 256, z, COLOR_RED)

def draw_bumper(obj):
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1/obj.scale.x, 1/obj.scale.y, 1/obj.scale.z)) # No scaling

    lineWidth = [(6, COLOR_BLACK), (2, COLOR_BLUE)]

    for width, color in lineWidth:
        bgl.glLineWidth(width)
        draw_cylinder((0,0,0), (0,0,0), 0.25, 0.75, 8, color)
        draw_cylinder((0,0,0.155), (0,0,0), 0.37, 0.0625, 8, color)
        draw_cylinder((0,0,0.535), (0,0,0), 0.37, 0.0625, 8, color)

    gpu.matrix.pop()
    
def draw_switch(obj):
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1/obj.scale.x, 1/obj.scale.y, 1/obj.scale.z)) # No scaling

    lineWidth = [(6, COLOR_BLACK), (2, COLOR_BLUE)]

    for width, color in lineWidth:
        bgl.glLineWidth(width)
        draw_cylinder((0,0,0), (0,0,0), 1.75, 0.25, 8, color)
        draw_cylinder((0,0,0.25), (0,0,0), 1.0, 0.1, 8, color)

    gpu.matrix.pop()

def get_draw_misc_col(col):
    def draw_misc(obj):
        gpu.matrix.push()

        gpu.matrix.multiply_matrix(obj.matrix_world)
        gpu.matrix.scale((1 / obj.scale.x, 1 / obj.scale.y, 1 / obj.scale.z))  # No scaling

        bgl.glLineWidth(6)
        draw_sphere((0.0, 0.0, 0.0), 0.2, COLOR_BLACK)
        draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), COLOR_BLACK)

        bgl.glLineWidth(2)
        draw_sphere((0.0, 0.0, 0.0), 0.2, (*col, 0.8))
        draw_arrow((0.0, 0.0, 0.0), (0.0, 1.5, 0.0), (*col, 0.8))

        gpu.matrix.pop()

    return draw_misc

def draw_vector_from_origin(obj, end_pos):
    gpu.matrix.push()
    gpu.matrix.multiply_matrix(obj.matrix_world)
    gpu.matrix.scale((1 / obj.scale.x, 1 / obj.scale.y, 1 / obj.scale.z))  # No scaling
    lineWidth = [(6, COLOR_BLACK), (2, COLOR_GREEN)]
    for (width, color) in lineWidth:
        coords = [(0.0, 0.0, 0.0), end_pos]
        bgl.glLineWidth(width)
        draw_batch(coords, color, 'LINES')
    gpu.matrix.pop()
    
name_map = {
    "[START]": draw_start,
    "[FALLOUT_VOLUME]": draw_fallout_volume,
    "[SAFE_VOLUME]": draw_safe_volume,
    "[MINE]": get_draw_misc_col(COLOR_RED),  # Red
    "[BUMPER]": get_draw_misc_col(COLOR_BLUE),  # Blue
    "[COLLECTIBLE]": get_draw_misc_col(COLOR_YELLOW),  # Yellow
    "[WORMHOLE]": get_draw_misc_col(COLOR_PURPLE),  # Purple
}
