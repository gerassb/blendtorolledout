import bpy


class StageData:
    def __init__(self):
        bpy.types.Scene.fallout_z = bpy.props.FloatProperty(default=-10.0)

    @property
    def fallout_z(self):
        return bpy.context.scene.fallout_z

    @fallout_z.setter
    def fallout_z(self, val):
        bpy.context.scene.fallout_z = val
