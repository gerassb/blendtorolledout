import os
# import tkinter as tk
# import tkinter.filedialog
import glob
import time
import uuid
import bpy
import bgl
import bpy_extras
import copy
import bmesh
# import cProfile, pstats, io
import pdb
import mathutils

from mathutils import Matrix, Vector, Quaternion
from . import config_generator, material_properties
from .descriptors import descriptors
from .stage_data import StageData
from . import obj_drawing
from . import ui
from . import statics
from bpy.props import BoolProperty, EnumProperty, PointerProperty, FloatProperty, IntProperty, FloatVectorProperty

stage_data = StageData()
mouse_pos = (0.0, 0.0)

file_base_name = "//"
want_quit_editor = False
export_tx = None
export_app = None


class OBJECT_OT_user_batch_export(bpy.types.Operator):
    bl_idname = "object.user_batch_export"
    bl_label = "Batch Export"
    bl_description = "Batch export stages"

    def execute(self, context):
        # Create the text block
        textblock = bpy.data.texts.get("blendtorolledout:batchexport")
        if textblock is None:
            textblock = bpy.data.texts.new("blendtorolledout:batchexport")
            textblock.from_string("""# Edit the config below and hit "Run Script" to perform the batch export
# Example config options have been provided as guidance
# NOTE THAT BACKSLASHES MUST BE DOUBLED UP (Or just use forward slashes)

# When Blender starts responding again, batch export will have finished
# If nothing appears to happen, double check the blend_dir

import bpy


# BEGIN CONFIG #
# Directory containing blend files to convert. If the directory is not valid, nothing will happen when script runs
blend_dir = "C:\\Path\\To\\Your\\BlendFile\\Folder" 
# Timestep of quaternion keyframe baking. Higher values decrease filesize, but might be less accurate
quat_bake_timestep = 2
# END CONFIG #

bpy.ops.object.b2ro_batch_export("INVOKE_DEFAULT", blend_dir=blend_dir, quat_bake_timestep=quat_bake_timestep)
    """)

        # Open the user prefs window
        bpy.ops.screen.userpref_show("INVOKE_DEFAULT")

        # Change area type to a text editor
        area = bpy.context.window_manager.windows[-1].screen.areas[0]
        area.type = "TEXT_EDITOR"
        area.spaces[0].text = textblock
        area.spaces[0].show_line_highlight = True
        area.spaces[0].show_line_numbers = True
        area.spaces[0].show_syntax_highlight = True
        bpy.ops.text.jump(line=1)  # Scroll to the top
        return {'FINISHED'}


class OBJECT_OT_b2ro_batch_export(bpy.types.Operator):
    bl_idname = "object.b2ro_batch_export"
    bl_label = "[DON'T USE THIS] Batch export Rolled Out! stages"
    # bl_description = ""

    blend_dir:  bpy.props.StringProperty()
    quat_bake_timestep = bpy.props.IntProperty()

    def execute(self, context):
        print("Initiating batch export")

        blends = glob.glob(self.blend_dir + "/*.blend")
        for file in blends:
            print("Opening: " + file)
            bpy.ops.wm.open_mainfile(filepath=file)

            bpy.context.scene.quat_bake_timestep = self.quat_bake_timestep

            dir = os.path.basename(file[:-6])
            print("Processing: " + dir)
            if not os.path.exists(os.path.join(self.blend_dir, dir)):
                os.makedirs(os.path.join(self.blend_dir, dir))

            bpy.ops.object.generate_obj("EXEC_DEFAULT")

            # Generates metadata since config generator copies it from the textbox
            create_metadata()
            
            conf_res = config_generator.generate_config(stage_data, "//model.obj")
            if conf_res.is_ok:
                with open(os.path.join(self.blend_dir, dir, "config.json"), "w") as f:
                    f.write(conf_res.unwrap())
            else: 
                print("Error in file: " + file)
                self.report({"ERROR"}, conf_res.unwrap_err())
                break
        return {'FINISHED'}

def create_metadata():
    textblock = bpy.data.texts.get("blendtorolledout:metadata")
    if textblock is None:
        textblock = bpy.data.texts.new("blendtorolledout:metadata")
        textblock.from_string("""# Edit the script below to modify stage metadata and more options
# Example options have been provided as guidance
# Lines beginning with a hash (#) are ignored

import textwrap

global stage_metadata
global stage_background_map

stage_metadata = {
    # A universally unique identifier (UUID) for this stage - Should be a version 4 UUID
    # A random UUID has been generated here for you
    "uuid": "%s",

    # A stage can have multiple names, the one shown in game depends on the current language setting
    "name": {
        # The "fallback" name is the name to be used when none of the other languages match
        "fallback": "Your colorful awesome stage!",

        # Example British English translation
        "en-GB": "Your colourful awesome stage!",

        # Example Japanese translation
        "ja": "カラフルなテストレベル",
    },

    # A stage can have multiple descriptions, the one shown in game depends on the current language setting
    "description": {
        # The "fallback" description is the description to be used when none of the other languages match
        "fallback": textwrap.dedent(\"\"\"\\
        Here's line one of a description.
        Here's some more description!
        What a lovely and colorful stage.\"\"\"),

        # Example British English translation
        "en-GB": textwrap.dedent(\"\"\"\\
        Here's line one of a description.
        Here's some more description!
        What a lovely and colourful stage.\"\"\"),
    },

    # A difficulty rating for the stage
    # Should be an integer value/whole number.
    "difficulty": 1,

    # A stage can have some tags attached to aid in searching
    # Tag names should always be English, lowercase, and have words separated by hyphens
    # (Eg: "my-awesome-tag", but not "My Awesome Tag")
    "tags": [
        "wire",
        "puzzle",
    ],

    # The stage creator's user ID
    # We don't really have those yet, so don't worry about this for now
    "creator_uuid": "11111111-1111-1111-1111-111111111111",
}

# The background environment to place the stage in
stage_background_map = "GrassyHillsMap"
    """ % str(uuid.uuid4()))
    return textblock

class OBJECT_OT_edit_metadata(bpy.types.Operator):
    bl_idname = "object.edit_metadata"
    bl_label = "Edit Stage Metadata"
    bl_description = "Edit the metadata associated with a stage"

    def execute(self, context):
        textblock = create_metadata()

        # Open the user prefs window
        bpy.ops.screen.userpref_show("INVOKE_DEFAULT")

        # Change area type to a text editor
        area = bpy.context.window_manager.windows[-1].screen.areas[0]
        area.type = "TEXT_EDITOR"
        area.spaces[0].text = textblock
        area.spaces[0].show_line_highlight = True
        area.spaces[0].show_line_numbers = True
        area.spaces[0].show_syntax_highlight = True
        bpy.ops.text.jump(line=1)  # Scroll to the top
        return {'FINISHED'}


class OBJECT_OT_create_new_empty_and_select(bpy.types.Operator):
    bl_idname = "object.create_new_empty_and_select"
    bl_label = "Create New Empty And Select"
    bl_description = "Creates a new empty object of a specified name"
    bl_options = {"UNDO"}
    name: bpy.props.StringProperty(default="Empty")
    switch_speed: bpy.props.FloatProperty(default=0.0)
    switch_model: bpy.props.StringProperty(default="pause")

    def execute(self, context):
        newEmpty = bpy.data.objects.new(self.name, None)
        newEmpty.location = bpy.context.scene.cursor.location
        bpy.context.collection.objects.link(newEmpty)
        newEmpty.empty_display_type = "PLAIN_AXES"
        bpy.context.view_layer.objects.active = newEmpty

        if bpy.context.mode == 'OBJECT':
            bpy.ops.object.select_all(action='DESELECT')
        else:
            bpy.context.object.select_set(False)

        if "[SW_" in newEmpty.name:
            newEmpty.switch_node_properties.playbackSpeed = self.switch_speed
            newEmpty.switch_node_properties.buttonVisual = self.switch_model

        newEmpty.select_set(True)
        return {'FINISHED'}

# Sets the interpolation of all quat keyframes to linear
def convert_action_to_linear(action):
    for fcurve in action.fcurves:
        fcurve_type = fcurve.data_path
        if fcurve_type == "rotation_quaternion":
            for key in fcurve.keyframe_points:
                key.interpolation = 'LINEAR'

class OBJECT_OT_drop_selected_coins(bpy.types.Operator):
    bl_idname = "object.drop_selected_coins"
    bl_label = "Drop Selected Coins"
    bl_description = "Drops the selected coins to a location 0.5 units above the nearest surface below the coin."
    bl_options = {"UNDO"}

    def execute(self, context):
        for coin in [obj for obj in context.selected_objects if '[COLLECTIBLE_' in obj.name]:
            cast = context.scene.ray_cast(context.view_layer.depsgraph, coin.location, Vector((0,0,-1)), distance=1000)
            if cast[0]:
                normal = cast[2]
                coin.location = cast[1]
                coin.location = coin.location + normal*0.5

                face_rotation = Vector.to_track_quat(normal, 'Z')
                coin.rotation_euler = face_rotation.to_euler()

        return {'FINISHED'}

def save_keyframes(fcurves):
    keyframe_lists = [None, None, None, None]
    pos_frames = [[], [], []]
    rot_euler_frames = [[], [], []]
    rot_quat_frames = [[], [], [], []]
    scale_frames = [[], [], []]

    for index in [0, 1, 2]:
        fcurve_pos = fcurves.find("location", index=index)
        fcurve_rot_euler = fcurves.find("rotation_euler", index=index)
        fcurve_scale = fcurves.find("scale", index=index)

        if fcurve_pos is not None:
            for keyframe in fcurve_pos.keyframe_points:
                add_keyframe_to_list(keyframe, pos_frames, index)

        if fcurve_rot_euler is not None:
            for keyframe in fcurve_rot_euler.keyframe_points:
                add_keyframe_to_list(keyframe, rot_euler_frames, index)


        if fcurve_scale is not None:
            for keyframe in fcurve_scale.keyframe_points:
                add_keyframe_to_list(keyframe, scale_frames, index)

    for index in [0, 1, 2, 3]:
        fcurve_rot_quat = fcurves.find("rotation_quaternion", index=index)
        if fcurve_rot_quat is not None:
            for keyframe in fcurve_rot_quat.keyframe_points:
                add_keyframe_to_list(keyframe, rot_quat_frames, index)

    keyframe_lists[0] = pos_frames
    keyframe_lists[1] = rot_euler_frames
    keyframe_lists[2] = rot_quat_frames
    keyframe_lists[3] = scale_frames

    return keyframe_lists

# Re-create the old keyframes
# If there's some easier/more efficient way to do this, please fix
def readd_keyframes(obj, keyframe_list, anim_type_list):
    obj_name = obj.name
    fcurves = obj.animation_data.action.fcurves
    for anim_type, group in anim_type_list:
        if keyframe_list[obj_name][anim_type] is not None:
            attribute = getattr(obj, group)
            for axis, value in enumerate(keyframe_list[obj_name][anim_type], 0):
                if keyframe_list[obj_name][anim_type][axis] is not None:
                    for key_index, keyframe in enumerate(keyframe_list[obj_name][anim_type][axis], 0):
                        # Add new keyframe with attribute / frame
                        attribute[axis] = keyframe["value"]
                        frame = keyframe["frame"]
                        setattr(obj, group, attribute)
                        obj.keyframe_insert(data_path=group, frame=frame, index=axis)
                        # Hackily restore interpolation, easing, handles, etc
                        fcurve = fcurves.find(group, index=axis)
                        new_key = fcurve.keyframe_points[key_index]
                        new_key.interpolation = keyframe["interpolation"]
                        new_key.easing = keyframe["easing"]
                        new_key.handle_left = Vector((keyframe["handle_left"].x, keyframe["handle_left"].y))
                        new_key.handle_left_type = keyframe["handle_left_type"]
                        new_key.handle_right = Vector((keyframe["handle_right"].x, keyframe["handle_right"].y))
                        new_key.handle_right_type = keyframe["handle_right_type"]

# Add keyframes to a list
def add_keyframe_to_list(keyframe, keyframe_list, axis):
    frame_data = {}
    frame_data["frame"] = keyframe.co[0]
    frame_data["value"] = keyframe.co[1]
    frame_data["interpolation"] = keyframe.interpolation
    frame_data["easing"] = keyframe.easing
    frame_data["handle_left"] = Vector((keyframe.handle_left.x, keyframe.handle_left.y))
    frame_data["handle_left_type"] = keyframe.handle_left_type
    frame_data["handle_right"] = Vector((keyframe.handle_right.x, keyframe.handle_right.y))
    frame_data["handle_right_type"] = keyframe.handle_right_type
    keyframe_list[axis].append(frame_data)

# Convert rotations to quaternions and export as a .json config, then restore original pos/rot/scale keyframes
class OBJECT_OT_generate_config(bpy.types.Operator):
    bl_idname = "object.generate_config"
    bl_label = "Generate Stage Config"
    bl_description = "Generate stage configuration file at the specified path"

    def execute(self, context):
        if bpy.data.texts.get("blendtorolledout:metadata") is None:
            self.report({"ERROR"}, "No metadata exists for this stage! Hit 'Edit metadata and more' to create some.")
            return {"CANCELLED"}

        start = time.perf_counter()

        original_keyframes = {}
        orig_rotation_mode = {}
        original_frame = context.scene.frame_current

        # Bake keyframes to add intermediate keyframes for quaternion conversion
        for obj in context.scene.objects:
            anim = obj.animation_data
            orig_rotation_mode[obj.name] = obj.rotation_mode
            has_euler_rot = False

            if anim is not None and anim.action is not None:
                fcurves = anim.action.fcurves
                has_euler_rot = (fcurves.find("rotation_euler", index=0) or fcurves.find("rotation_euler", index=1) or fcurves.find("rotation_euler", index=2))
                if has_euler_rot: obj.rotation_mode = "XYZ"

            # Skip baking if it's unnecessary
            if anim is None or anim.action is None or not has_euler_rot:
                obj.rotation_mode = "QUATERNION"
                print("Object " + str(obj) + " has no animation, action, or rotation keyframes, skipping...")
                continue

            # Saves position and scale keyframes (if they exist) for later restoration
            # It doesn't seem like keyframes can easily be copied as a whole - so all their values are copied
            original_keyframes[obj.name] = save_keyframes(fcurves) 

            # Save the start and end local transformation matrices to ensure first and last baked frames are correct
            context.scene.frame_set(original_frame)
            start_rot = copy.copy(obj.rotation_euler) 
            context.scene.frame_set(context.scene.frame_end)
            end_rot = copy.copy(obj.rotation_euler)

            # Determine the first and last frame for baking based on rotation fcurves
            frame_start = context.scene.frame_end
            frame_end = 0

            for index in [0, 1, 2]:
                fcurve = fcurves.find("rotation_euler", index=index)
                if fcurve is not None:
                    curve_start_frame = int(fcurve.keyframe_points[0].co[0])
                    curve_end_frame = int(fcurve.keyframe_points[len(fcurve.keyframe_points)-1].co[0])
                    if curve_start_frame < frame_start: frame_start = curve_start_frame
                    if curve_end_frame > frame_end: frame_end = curve_end_frame
                    
            frame_step = context.scene.quat_bake_timestep

            # Only bake current object to account for varying start/end frames
            bpy.ops.object.select_all(action='DESELECT')
            obj.select_set(True)

            context_override = bpy.context.copy()
            context_override['selected_editable_objects'] = [obj] 

            print("Baking keyframes for object " + str(obj) + " from " + str(frame_start) + " to " + str(frame_end) + " at a timestep of " + str(frame_step))

            bpy.ops.nla.bake(context_override,
                frame_start=frame_start,
                frame_end=frame_end,
                step=frame_step,
                bake_types={'OBJECT'}
            )

            # Restore starting and ending rotation frames for conversion 
            context.scene.frame_set(original_frame)
            obj.rotation_euler = start_rot 
            obj.keyframe_insert("rotation_euler", options={'INSERTKEY_NEEDED'})

            context.scene.frame_set(context.scene.frame_end)
            obj.rotation_euler = end_rot 
            obj.keyframe_insert("rotation_euler", options={'INSERTKEY_NEEDED'})

            # Convert baked Euler rotation keyframes to quaternion
            fcurves = anim.action.fcurves
            frames_list = []
            for index in [0, 1, 2]:
                fc = fcurves.find("rotation_euler", index=index)
                # Make a list of frames that have euler rot keyframes
                if fc:
                    frames_list.extend([kfp.co[0] for kfp in fc.keyframe_points])

            for f in set(frames_list):
                context.scene.frame_set(init(f))
                euler_rot = obj.rotation_euler
                quat_rot = euler_rot.to_quaternion()
                obj.rotation_quaternion = quat_rot
                obj.keyframe_insert("rotation_quaternion", frame=f)

            obj.rotation_mode = "QUATERNION"

            # Restore original position and scale keyframes to reduce filesize
            print("Restoring original position and scale keyframes for " + obj.name)
            for nonrotation_fcurve in [fcurve for fcurve in fcurves if fcurve.data_path != "rotation_quaternion" ]:
                #print("removed " + nonrotation_fcurve.data_path)
                fcurves.remove(nonrotation_fcurve)

            readd_keyframes(obj, original_keyframes, [(0, "location"), (3, "scale")])

        context.scene.frame_set(original_frame) 

        file_base_name = "//" + os.path.splitext(os.path.basename(bpy.context.blend_data.filepath))[0] + "/"
        config_name = file_base_name + "config.json"

        conf_res = config_generator.generate_config(stage_data, "//model.obj")

        if conf_res.is_ok:
            os.makedirs(bpy.path.abspath(file_base_name), exist_ok=True)
            with open(bpy.path.abspath(config_name), "w") as f:
                f.write(conf_res.unwrap())
                self.report({"INFO"}, "Wrote config")
        else:
            self.report({"ERROR"}, conf_res.unwrap_err())

        print("Exported to " + bpy.path.abspath(config_name))


        # Restore original keyframes
        for obj_name in original_keyframes.keys():
            obj = context.scene.objects[obj_name]
            fcurves = obj.animation_data.action.fcurves

            print("Restoring original Euler rotation keyframes for " + obj.name)
            # Remove newly baked frames
            for rotation_fcurve in [fcurve for fcurve in fcurves if fcurve.data_path == "rotation_quaternion" ]:
                #print("removed " + rotation_fcurve.data_path)
                fcurves.remove(rotation_fcurve)

            readd_keyframes(obj, original_keyframes, [(1, "rotation_euler")])

            for fcurve in fcurves:
                fcurve.update()

            obj.rotation_mode = "XYZ"

        # Restore original rotation mode
        for obj in context.scene.objects: 
            obj.rotation_mode = orig_rotation_mode[obj.name]

        end = time.perf_counter()
        print("Config generation took %.3gs" % (end-start))

        context.scene.frame_set(original_frame)
        return{'FINISHED'}


class OBJECT_OT_generate_obj(bpy.types.Operator):
    bl_idname = "object.generate_obj"
    bl_label = "Generate OBJ"
    bl_description = "Generate OBJ model file"

    def execute(self, context):
        # Set object mode so UV maps can be modified
        bpy.ops.object.mode_set(mode='OBJECT')

        origin_frame = context.scene.frame_current
        start_frame = context.scene.frame_start
        # OBJ export sets frame to start frame
        context.scene.frame_start = origin_frame

        file_base_name = "//" + os.path.splitext(os.path.basename(bpy.context.blend_data.filepath))[0] + "/"
        obj_name = file_base_name + "model.obj"
        os.makedirs(bpy.path.abspath(file_base_name), exist_ok=True)

        # Sets frame to start
        context.scene.frame_set(origin_frame)

        # Ignore empties
        translation_objects = [obj for obj in context.view_layer.objects]

        orig_matrix_dict = {}
        orig_rotation_mode = {}
        original_keyframes = {}

        for obj in translation_objects:
            orig_rotation_mode[obj.name] = obj.rotation_mode
            obj.rotation_mode = 'QUATERNION'
            print("\tMoving object " + obj.name + " to origin for export")
            orig_matrix_dict[obj.name] = copy.copy(obj.matrix_local)

            # Blender's OBJ export moves things to their loc/rot/scale on export
            # This saves a keyframe at the export frame of the loc/rot/scale at the origin and with
            # no rotation, so it's properly exported. If there is a keyframe at the export keyframe,
            # the pos/rot/scale is stored, and then the keyframe is re-created.
            
            if obj.animation_data is not None and obj.animation_data.action is not None:
                fcurves = obj.animation_data.action.fcurves

                original_keyframes[obj.name] = save_keyframes(fcurves) 
                for fcurve in [f for f in fcurves if f.data_path in ('location', 'rotation_euler', 'rotation_quaternion', 'scale')]:
                    fcurves.remove(fcurve)

            #For non-animated objects
            obj.matrix_local = Matrix.Identity(4)
            
        # Modulo UV face position by their texture size to prevent floating-point imprecision
        bmesh_to_mesh = {}
        original_uv_loops = {}

        print("Fixing up UV coordinates...")
        for obj in translation_objects:
            if obj.data is not None and obj.type == 'MESH':
                bm = bmesh.new()
                bm.from_mesh(obj.data)
                uv_layer = bm.loops.layers.uv.verify()

                bmesh_to_mesh[bm] = obj.data
                original_uv_loops[bm] = {}

                bm.faces.index_update()
                for face in bm.faces:
                    face.loops.index_update()

                    original_uv_loops[bm][face.index] = {}
                    uv_loops = []

                    for loop in face.loops:
                        # Would have liked to implement this with a dict of loops, but
                        # Blender's API prevents Vector from being hashed/used as a mutable key
                        uv = loop[uv_layer].uv
                        original_uv_loops[bm][face.index][loop.index] = Vector((uv.x, uv.y))
                        uv_loops.append(uv)

                    if len(uv_loops) > 0: 
                        avg_x = sum(v[0] for v in uv_loops)/len(uv_loops)
                        avg_y = sum(v[1] for v in uv_loops)/len(uv_loops)
                        delta_x = avg_x - (avg_x % 1)
                        delta_y = avg_y - (avg_y % 1)
                        
                        for loop in uv_loops:
                            loop.x -= delta_x
                            loop.y -= delta_y

                bm.to_mesh(obj.data)

        bpy.ops.export_scene.obj(
            filepath=bpy.path.abspath(obj_name),
            use_triangles=True,
            path_mode="RELATIVE",
            axis_forward="X",
            axis_up="Y"
        )
        print("Exported to" + bpy.path.abspath(obj_name))

        # Restore the original UV coordinates
        print("Restoring UV coordinates...")
        for bm in bmesh_to_mesh:
            uv_layer = bm.loops.layers.uv.verify()

            for face in bm.faces:
                face.loops.index_update()
                for loop_idx, loop in original_uv_loops[bm][face.index].items():
                    face.loops[loop_idx][uv_layer].uv = loop

            bm.to_mesh(bmesh_to_mesh[bm])
            bm.free()
        
        # Restore original position and animation
        for obj in translation_objects:
            print("\tRestoring position and animation of " + obj.name)
            if obj.name in original_keyframes.keys():
                readd_keyframes(obj, original_keyframes, [(0, "location"), (1, "rotation_euler"), (2, "rotation_quaternion"), (3, "scale")])

            obj.matrix_local = orig_matrix_dict[obj.name]
            obj.rotation_mode = orig_rotation_mode[obj.name]
            context.scene.frame_start = start_frame
        return {'FINISHED'}


class VIEW3D_PT_stage_edit_panel(bpy.types.Panel):
    bl_idname = "VIEW3D_PT_stage_edit_panel"
    bl_label = "BlendToRolledOut"
    bl_category = "BlendToRolledOut"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context_mode = 'OBJECT'

    def draw(self, context):
        file_base_name = "//" + os.path.splitext(os.path.basename(bpy.context.blend_data.filepath))[0] + "/"
        config_name = file_base_name + "config.json"
        obj_name = file_base_name + "model.obj"

        layout = self.layout

        column_stage = layout.column()
        column_stage.label(text="Stage")
        column_stage.operator("OBJECT_OT_edit_metadata", text="Edit metadata and more")

        layout.separator()
        column_add = layout.column()
        column_add.label(text="Add Stage Object")
        newStart = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Starting Position")
        newStart.name = "[START]"

        goal_row = column_add.row(align=True)
        new_goal_b = goal_row.operator("OBJECT_OT_create_new_empty_and_select", text="Goal (B)")
        new_goal_b.name = "[GOAL_B] Blue goal"
        new_goal_g = goal_row.operator("OBJECT_OT_create_new_empty_and_select", text="Goal (G)")
        new_goal_g.name = "[GOAL_G] Green goal"
        new_goal_r = goal_row.operator("OBJECT_OT_create_new_empty_and_select", text="Goal (R)")
        new_goal_r.name = "[GOAL_R] Red goal"

        new_bumper = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Bumper")
        new_bumper.name = "[BUMPER]"
        new_mine = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Mine")
        new_mine.name = "[MINE]"

        collectible_row = column_add.row(align=True)
        new_collectible_1 = collectible_row.operator("OBJECT_OT_create_new_empty_and_select", text="Coll. x1")
        new_collectible_1.name = "[COLLECTIBLE_1]"
        new_collectible_1 = collectible_row.operator("OBJECT_OT_create_new_empty_and_select", text="Coll. x5")
        new_collectible_1.name = "[COLLECTIBLE_5]"
        new_collectible_10 = collectible_row.operator("OBJECT_OT_create_new_empty_and_select", text="Coll. x10")
        new_collectible_10.name = "[COLLECTIBLE_10]"

        new_wormhole = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Wormhole")
        new_wormhole.name = "[WH]"
        new_fallout_volume = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Fallout Volume")
        new_fallout_volume.name = "[FALLOUT_VOLUME]"
        new_safe_volume = column_add.operator("OBJECT_OT_create_new_empty_and_select", text="Safe Volume")
        new_safe_volume.name = "[SAFE_VOLUME]"

        layout.separator()
        column_switches = layout.column()
        column_switches.label(text="Switches")

        switch_row = column_switches.row(align=True)
        new_switch_frw = switch_row.operator("OBJECT_OT_create_new_empty_and_select", text="<<")
        new_switch_frw.name = "[SW_FAST_RW]"
        new_switch_frw.switch_speed = -2.0
        new_switch_frw.switch_model = "fast_backward"
        new_switch_rw = switch_row.operator("OBJECT_OT_create_new_empty_and_select", text="<")
        new_switch_rw.name = "[SW_RW]"
        new_switch_rw.switch_speed = -1.0
        new_switch_rw.switch_model = "backward"
        new_switch_pause = switch_row.operator("OBJECT_OT_create_new_empty_and_select", text="||")
        new_switch_pause.name = "[SW_PAUSE]"
        new_switch_pause.switch_speed = 0.0
        new_switch_pause.switch_model = "pause"
        new_switch_fw = switch_row.operator("OBJECT_OT_create_new_empty_and_select", text=">")
        new_switch_fw.name = "[SW_FW]"
        new_switch_fw.switch_speed = 1.0
        new_switch_fw.switch_model = "forward" 
        new_switch_ffw = switch_row.operator("OBJECT_OT_create_new_empty_and_select", text=">>")
        new_switch_ffw.name = "[SW_FAST_FW]"
        new_switch_ffw.switch_speed = 2.0
        new_switch_ffw.switch_model = "fast_forward"

        layout.separator()
        column_gravity = layout.column()
        column_gravity.label(text="Gravity")

        column_gravity_disabled = column_gravity.column()
        column_gravity_disabled.enabled = False

        column_gravity_disabled.operator("object.select_all", text="Gravity Point Attractor [NYI]")
        column_gravity_disabled.operator("object.select_all", text="Gravity Point Repeller [NYI]")
        column_gravity_disabled.operator("object.select_all", text="Gravity Directional Volume [NYI]")

        layout.separator()
        column_selected_objs = layout.column()
        column_selected_objs.label(text="Active Object")
        
        # Display fancy UI properties
                            
        if context.active_object is not None:
            obj = context.active_object
            column_selected_objs.label(text="Object: " + obj.name)
            
            column_selected_objs.prop(obj, "animation_id")
            column_selected_objs.prop(obj, "animation_init_timescale")
            column_selected_objs.prop(obj, "animation_loop")

            is_descriptor = False
            propertyGroup = obj.mesh_node_properties

            for desc in descriptors.descriptors:
                if "[START]" not in obj.name and desc.get_object_name() in obj.name:
                        is_descriptor = True
                        propertyGroup = desc.get_node_properties(obj)
                        break
            
            if propertyGroup is not None:
                for ui_prop in propertyGroup.__annotations__.keys():
                    if ui_prop in ['seesawSensitivity', 'seesawSpringiness', 'seesawFriction', 'rotationAxis']:
                        if not getattr(propertyGroup, 'seesawSurface'): continue
                    if ui_prop in ['touchPlatformGroup', 'touchPlatformTrigger', 'touchPlatformGroupType']:
                        if not getattr(propertyGroup, 'touchPlatform'): continue
                    if ui_prop in ['springSurfaceImpulse']:
                        if not getattr(propertyGroup, 'springSurface'): continue
                    if ui_prop in ['conveyorSurfaceVelocity', 'conveyorSurfaceUVVelocity']:
                        if not getattr(propertyGroup, 'conveyorSurface'): continue
                    if ui_prop in ['windVolumeVelocity']:
                        if not getattr(propertyGroup, 'windVolume'): continue
                    if ui_prop in ['gravitySurfaceLineTrace']:
                        if not getattr(propertyGroup, 'gravitySurface'): continue

                    column_selected_objs.prop(propertyGroup, ui_prop)

        layout.separator()
        column_export = layout.column()
        column_export.label(text="Export")

        column_export.prop(context.scene, "stage_timer_seconds")
        column_export.prop(context.scene, "quat_bake_timestep")
        column_export.operator("OBJECT_OT_generate_config", text="Generate config at " + config_name)
        column_export.operator("OBJECT_OT_generate_obj", text="Generate OBJ at " + obj_name)
        column_export.operator("OBJECT_OT_user_batch_export", text="Batch Export")
        column_export.prop(context.scene, "texture_path_override")

        layout.separator()
        column_settings = layout.column()
        column_settings.label(text="Settings/Misc")
        column_settings.operator("VIEW3D_OT_draw_stage_objects")
        column_settings.prop(context.scene, "draw_objects")
        column_settings.operator("OBJECT_OT_drop_selected_coins")


class VIEW3D_OT_draw_stage_objects(bpy.types.Operator):
    bl_idname = "view3d.draw_stage_objects"
    bl_label = "Draw Stage Objects"
    bl_description = "Whether or not visual representations of stage objects should be drawn"
    bl_options = {"UNDO"}

    def modal (self, context, event):
        context.area.tag_redraw()
        return {"PASS_THROUGH"}

    def execute(self, context):
        global draw_handlers

        if context.scene.draw_objects and context.area.type == "VIEW_3D":
            # The arguments we pass to the callback
            args = (self, context)
            # Adds the region drawing callback
            self._handle_3d = bpy.types.SpaceView3D.draw_handler_add(draw_callback_3d, args, "WINDOW", "POST_VIEW")

            statics.active_draw_handlers.append(self._handle_3d)

            context.window_manager.modal_handler_add(self)

            return {"RUNNING_MODAL"}
        else:
            self.report({"WARNING"}, "View3D not found, or stage visibility toggled off.")
            return {"CANCELLED"}


class MATERIAL_PT_ro_material(bpy.types.Panel):
    bl_idname = "MATERIAL_PT_ro_material"
    bl_label = "Rolled Out Material Settings"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        return (context.object.active_material is not None)

    def draw(self, context):
        layout = self.layout
        
        layout.use_property_split = True
        layout.use_property_decorate = True

        mat = context.material
        
        layout.prop(mat, "ro_material_type")
        for prop in material_properties.mat_props_ro_material[getattr(mat, 'ro_material_type')]:
            layout.prop(mat, prop)
        layout.prop(mat, "animation_loop")

        layout.separator()

        layout.prop(mat, "ro_attach_to_touch_platform_group")
        if "ro_attach_to_touch_platform_group" in mat and mat["ro_attach_to_touch_platform_group"]:
            layout.prop(mat, "ro_touch_platform_group_id")

def draw_callback_3d(self, context):
    bgl.glEnable(bgl.GL_BLEND)
    bgl.glEnable(bgl.GL_DEPTH_TEST)
    if context.scene.draw_objects:
        for obj in context.scene.objects:
            if obj.visible_get():
                for desc in descriptors.descriptors:
                    if desc.get_object_name() in obj.name:
                        desc.render(obj)

        # Draw conveyor arrow
        conveyorObjects = [obj for obj in context.view_layer.objects if "ro:vector:conveyorSurfaceVelocity" in obj.keys() and obj.visible_get()]
        # TODO: conveyorUVObjects = [obj for obj in context.view_layer.objects if "ro:vector:conveyorSurfaceUVVelocity" in obj.keys() and obj.visible_get()]
        windVolumeObjects = [obj for obj in context.view_layer.objects if "ro:vector:windVolumeVelocity" in obj.keys() and obj.visible_get()]
        
        springObjects = [obj for obj in context.view_layer.objects if "ro:vector:springSurfaceImpulse" in obj.keys() and obj.visible_get()]

        for conveyorObject in conveyorObjects:
            conveyorSurfaceVector = conveyorObject["ro:vector:conveyorSurfaceVelocity"]
            conveyorSurfaceEndPos = (conveyorSurfaceVector[0]/500, conveyorSurfaceVector[1]/500, conveyorSurfaceVector[2]/500)
            obj_drawing.draw_vector_from_origin(conveyorObject, conveyorSurfaceEndPos)
            
        # TODO: for conveyorObject in conveyorUVObjects:

        for springObject in springObjects: 
            springVector = springObject["ro:vector:springSurfaceImpulse"]
            springEndPos = (springVector[0]/500, springVector[1]/500, springVector[2]/500)
            obj_drawing.draw_vector_from_origin(springObject, springEndPos)

        for windVolumeObject in windVolumeObjects:
            windVolumeVector = windVolumeObject["ro:vector:windVolumeVelocity"]
            windVolumeEndPos = (windVolumeVector[0]/500, windVolumeVector[1]/500, windVolumeVector[2]/500)
            obj_drawing.draw_vector_from_origin(windVolumeObject, windVolumeEndPos)

# Updates object keys as a result of changing an RNA property
def update_prop(self, context, prop):
    if context.active_object is not None:
        prop_name = prop
        prop_value = getattr(self, prop)
        # Assuming 'flags' is just referring to the type of key for now
        prop_flags = ""
        if isinstance(prop_value, bool):
            prop_flags = "bool"
        elif isinstance(prop_value, tuple):
            prop_flags = "enum"
        elif isinstance(prop_value, float):
            prop_flags = "float"
        elif isinstance(prop_value, str):
            prop_flags = "string"
        elif isinstance(prop_value, int):
            prop_flags = "int"
        elif prop_name == "linkedNode":
            prop_flags = "node"
        elif "Impulse" or "Velocity" in prop_name:
            prop_flags = "vector"

        prop_key = "ro:{}:{}".format(prop_flags, prop_name)

        context.active_object[prop_key] = getattr(self, prop)

        if (prop == "seesawSurface"):
            for seesawProperty in ["ro:string:rotationAxis", "ro:float:seesawSensitivity", "ro:float:seesawSpringiness", "ro:float:seesawFriction"]:
                prop_segments = seesawProperty.split(":")
                prop_name = prop_segments[2]
                if not (seesawProperty in context.active_object.keys()):
                    context.active_object[seesawProperty] = getattr(context.active_object.mesh_node_properties, prop_name)
                
                elif getattr(self, prop) == False:
                    del context.active_object[seesawProperty]

        if "touch_platform_groups" not in context.scene.keys():
            context.scene["touch_platform_groups"] = {}

        touch_platform_groups = context.scene["touch_platform_groups"]

        if (prop == "touchPlatform"):
            if getattr(self, prop):
                current_id = getattr(self, "touchPlatformGroup")
                context.active_object["ro:int:touchPlatformGroup"] = getattr(self, "touchPlatformGroup")
                touch_platform_groups[str(current_id)] = int(getattr(self, "touchPlatformGroupType"))
                context.active_object["ro:string:touchPlatformGroupType"] = getattr(self, "touchPlatformGroupType")
                context.active_object["ro:bool:touchPlatformTrigger"] = getattr(self, "touchPlatformTrigger")
            else:
                if "ro:bool:touchPlatformTrigger" in context.active_object.keys():
                    del context.active_object["ro:bool:touchPlatformTrigger"]
                del context.active_object["ro:bool:touchPlatform"]
                del context.active_object["ro:int:touchPlatformGroup"]
                del context.active_object["ro:string:touchPlatformGroupType"]

        if (prop == "touchPlatformGroup"):
            if getattr(self, "touchPlatform") == 1:
                for group_id in touch_platform_groups.keys():
                    if group_id == str(getattr(self, "touchPlatformGroup")):
                        setattr(self, "touchPlatformGroupType", str(touch_platform_groups[str(group_id)]))
                        context.active_object["ro:string:touchPlatformGroupType"] = touch_platform_groups[str(group_id)]
                        return
                touch_platform_groups[str(getattr(self, prop))] = int(getattr(self, "touchPlatformGroupType"))

        if (prop == "touchPlatformGroupType"):
            if getattr(self, "touchPlatform") == 1:
                group_id = str(getattr(self, "touchPlatformGroup"))
                touch_platform_groups[group_id] = int(getattr(self, prop))
                for obj in context.scene.objects:
                    if "ro:int:touchPlatformGroup" in obj.keys() and obj["ro:int:touchPlatformGroup"] == int(group_id):
                        group_type = obj.get("ro:string:touchPlatformGroupType", None)
                        if group_type != str(touch_platform_groups[group_id]):
                            obj["ro:string:touchPlatformGroupType"] = str(touch_platform_groups[group_id])
                            obj.mesh_node_properties.touchPlatformGroupType = str(touch_platform_groups[group_id])

        if (prop == "springSurface"):
            if not ("ro:vector:springSurfaceImpulse" in context.active_object.keys()):
                context.active_object["ro:vector:springSurfaceImpulse"] = getattr(context.active_object.mesh_node_properties, "springSurfaceImpulse")
            elif getattr(self, prop) == False:
                del context.active_object["ro:vector:springSurfaceImpulse"]
                
        if (prop == "conveyorSurface"):
            if not ("ro:vector:conveyorSurfaceVelocity" in context.active_object.keys()) or not ("ro:vector:conveyorSurfaceUVVelocity" in context.active_object.keys()):
                context.active_object["ro:vector:conveyorSurfaceVelocity"] = getattr(context.active_object.mesh_node_properties, "conveyorSurfaceVelocity")
                context.active_object["ro:vector:conveyorSurfaceUVVelocity"] = getattr(context.active_object.mesh_node_properties, "conveyorSurfaceUVVelocity")
            elif getattr(self, prop) == False:
                del context.active_object["ro:vector:conveyorSurfaceVelocity"]
                del context.active_object["ro:vector:conveyorSurfaceUVVelocity"]
                
        if (prop == "windVolume"):
            if not ("ro:vector:windVolumeVelocity" in context.active_object.keys()):
                context.active_object["ro:vector:windVolumeVelocity"] = getattr(context.active_object.mesh_node_properties, "windVolumeVelocity")
            elif getattr(self, prop) == False:
                del context.active_object["ro:vector:windVolumeVelocity"]
                            
# List of attributes that can be assigned to an object
class MeshNodeProperties(bpy.types.PropertyGroup):
    exclude: BoolProperty(name="Exclude from Config", 
                        update=lambda s,c: update_prop(s, c,"exclude"),
                        default=False)   #TODO: this isn't implemented?
    gravitySurface: BoolProperty(name="Gravity Surface", 
                        update=lambda s,c: update_prop(s, c,"gravitySurface"),
                        default=False)
    gravitySurfaceLineTrace: BoolProperty(name="Gravity Surface Line Trace", 
                        update=lambda s,c: update_prop(s, c,"gravitySurfaceLineTrace"),
                        default=True)                        
    iceSurface: BoolProperty(name="Ice Surface", 
                        update=lambda s,c: update_prop(s, c,"iceSurface"),
                        default=False)
    seesawSurface: BoolProperty(name="Seesaw Surface", 
                        update=lambda s,c: update_prop(s, c,"seesawSurface"),
                        default=False)                        
    # Seesaw axes are swapped so Blender's coordinate system aligns with Unreal's
    # TODO: Visual seesaw axis indicator...
    rotationAxis: EnumProperty(name="Seesaw Rotation Axis",
                        update=lambda s,c: update_prop(s, c,"rotationAxis"),
                        items=[("x", "X Axis", ""), 
                               ("y", "Y Axis", "")]
                        )
    seesawSensitivity: FloatProperty(name="Seesaw Sensitivity",
                        update=lambda s,c: update_prop(s, c,"seesawSensitivity"),
                        default=0.7)
    seesawSpringiness: FloatProperty(name="Seesaw Springiness",
                        update=lambda s,c: update_prop(s, c,"seesawSpringiness"),
                        default=0.9)
    seesawFriction: FloatProperty(name="Seesaw Friction",
                        update=lambda s,c: update_prop(s, c,"seesawFriction"),
                        default=1.5)

    touchPlatform: BoolProperty(name="Touch Platform",
                        update=lambda s,c: update_prop(s, c,"touchPlatform"),
                        default=False)
    touchPlatformGroup: IntProperty(name="Touch Platform Group", 
                        update=lambda s,c: update_prop(s, c, "touchPlatformGroup"),
                        default=0)
    touchPlatformGroupType: EnumProperty(name = "Group Type", 
                        update=lambda s,c: update_prop(s, c, "touchPlatformGroupType"),
                        items=[("0", "Play Once", ""), 
                               ("1", "Loop", ""),
                               ("2", "Ping Pong", "")])
    touchPlatformTrigger: BoolProperty(name="Triggers Platform Group",
                                       update=lambda s,c: update_prop(s,c,"touchPlatformTrigger"),
                                       default=True)

    springSurface: BoolProperty(name="Spring Surface",
                        update=lambda s,c: update_prop(s, c,"springSurface"),
                        default=False)
    springSurfaceImpulse: FloatVectorProperty(name="Spring Impulse (X, Y, Z)",
                        update=lambda s,c: update_prop(s, c,"springSurfaceImpulse"),
                        default=(0,0,0))
                                
    stickySurface: BoolProperty(name="Sticky Surface",
                        update=lambda s,c: update_prop(s, c,"stickySurface"),
                        default=False)
    quicksandSurface: BoolProperty(name="Quicksand Surface",
                        update=lambda s,c: update_prop(s, c, "quicksandSurface"),
                        default=False)

    conveyorSurface: BoolProperty(name="Conveyor Surface",
                        update=lambda s,c: update_prop(s, c,"conveyorSurface"),
                        default=False)
    conveyorSurfaceVelocity: FloatVectorProperty(name="Conveyor Surface Local Velocity",
                        update=lambda s,c: update_prop(s, c,"conveyorSurfaceVelocity"),
                        default=(0,0,0))
    conveyorSurfaceUVVelocity: FloatVectorProperty(name="Conveyor Surface UV Velocity",
                        update=lambda s,c: update_prop(s, c,"conveyorSurfaceUVVelocity"),
                        default=(0,0), size=2)

    falloutVolume: BoolProperty(name="Fallout Volume",
                        update=lambda s,c: update_prop(s, c,"falloutVolume"),
                        default=False)                     
    windVolume: BoolProperty(name="Wind Volume",
                        update=lambda s,c: update_prop(s, c,"windVolume"),
                        default=False)
    windVolumeVelocity: FloatVectorProperty(name="Wind Volume Velocity",
                        update=lambda s,c: update_prop(s, c,"windVolumeVelocity"),
                        default=(0,0,0))
    collisionEnabled: BoolProperty(name="Collision Enabled",
                        update=lambda s,c: update_prop(s, c, "collisionEnabled"),
                        default=True)

# List of attributes that can be assigned to a mechanic node
class WormholeNodeProperties(bpy.types.PropertyGroup):                        
    linkedNode: PointerProperty(name="Linked Object", 
                                  type=bpy.types.Object, 
                                  update=lambda s, c: update_prop(s, c, "linkedNode"))                                  
    gravityAlignSelf: BoolProperty(name="Align Gravity to Self",
                        update=lambda s,c: update_prop(s, c,"gravityAlignSelf"),
                        default=False)
    gravityAlignLinked: BoolProperty(name="Align Gravity to Linked",
                        update=lambda s,c: update_prop(s, c,"gravityAlignLinked"),
                        default=False)

class SwitchNodeProperties(bpy.types.PropertyGroup):
    initiallyPressed: BoolProperty(name="Initially Pressed",
                        update=lambda s,c: update_prop(s, c, "initiallyPressed"),
                        default=False)
    playbackSpeed:    FloatProperty(name="Playback Speed",
                        update=lambda s,c: update_prop(s, c, "playbackSpeed"),
                        default=0.0)
    linkedNodeId:     IntProperty(name="Linked ID",
                        update=lambda s,c: update_prop(s, c, "linkedNodeId"),
                        default=0)
    buttonVisual:     EnumProperty(name="Model",
                        update=lambda s,c: update_prop(s, c, "buttonVisual"),
                        items=[("pause", "Pause", ""), 
                               ("forward", "Forward", ""),
                               ("backward", "Backward", ""),
                               ("fast_forward", "Fast Forward", ""),
                               ("fast_backward", "Fast Backward", "")])

