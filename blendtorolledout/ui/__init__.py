import blf

from . import coreui


DRAW_CURSOR_FRAME_BEGIN = [20.0, 28.0]
draw_cursor = DRAW_CURSOR_FRAME_BEGIN.copy()
viewport_size = (0.0, 0.0)
mouse_pos = (0.0, 0.0)
prev_object_size = (0.0, 0.0)
prev_object_padding = (0.0, 0.0)

want_capture_mouse = False
is_mouse_clicked = False
is_mouse_down = False
is_shift_pressed = False
mouse_down_pos = (0.0, 0.0)


def begin_frame(in_viewport_size, in_mouse_pos):
    global draw_cursor
    global mouse_pos
    global viewport_size
    global want_capture_mouse

    mouse_pos = coreui.pos_to_viewport_space(in_mouse_pos, in_viewport_size)
    draw_cursor = DRAW_CURSOR_FRAME_BEGIN.copy()
    viewport_size = in_viewport_size
    want_capture_mouse = False


def end_frame():
    global is_mouse_clicked

    if is_mouse_clicked:
        is_mouse_clicked = False


def mouse_pressed():
    global is_mouse_down
    global mouse_down_pos

    is_mouse_down = True
    mouse_down_pos = mouse_pos


def mouse_released():
    global is_mouse_down
    global is_mouse_clicked

    is_mouse_down = False

    if mouse_pos == mouse_down_pos:
        is_mouse_clicked = True


def same_line():
    global draw_cursor
    global prev_object_size

    draw_cursor[0] += prev_object_size[0] + prev_object_padding[0] + 4.0
    draw_cursor[1] -= prev_object_size[1] + prev_object_padding[1]


def reset_line():
    draw_cursor[0] = DRAW_CURSOR_FRAME_BEGIN[0]


def label(text: str):
    global draw_cursor
    global viewport_size
    global prev_object_size
    global prev_object_padding

    size = (80.0, 20.0)

    pos = coreui.pos_to_viewport_space(draw_cursor, viewport_size)

    coreui.draw_text_2d((1.0, 1.0, 1.0, 1.0), text, (pos[0] + 2, pos[1] - 14))

    draw_cursor[1] += size[1] + 4.0
    prev_object_size = size
    prev_object_padding = (0.0, 4.0)


def button(text: str):
    global draw_cursor
    global viewport_size
    global prev_object_size
    global prev_object_padding
    global want_capture_mouse

    blf.size(0, 12, 72)
    text_width = blf.dimensions(0, text)[0]
    size = (text_width + 4.0, 20.0)

    mouse_over = coreui.is_point_over_area(mouse_pos, draw_cursor, size)
    color = (0.259, 0.525, 0.957, 1.0) if mouse_over else (0.059, 0.325, 0.757, 0.8)

    if mouse_over:
        want_capture_mouse = True

    pos = coreui.pos_to_viewport_space(draw_cursor, viewport_size)
    draw_size = coreui.size_to_viewport_space(size)

    coreui.draw_box_2d(*pos, *draw_size, color=color)  # Blue
    coreui.draw_text_2d((1.0, 1.0, 1.0, 1.0), text, (pos[0] + 2, pos[1] - 14))

    draw_cursor[1] += size[1] + 4.0
    prev_object_size = size
    prev_object_padding = (0.0, 4.0)

    class ButtonState:
        def __init__(self):
            self.mouse_over = mouse_over
            self.clicked = mouse_over and is_mouse_clicked

        def __bool__(self):
            return self.clicked

    return ButtonState()


def slider(text: str, value, step):
    global mouse_down_pos

    if is_shift_pressed:
        step *= 0.1

    if button("<"):
        value -= step

    same_line()

    if button(">"):
        value += step

    same_line()

    if button("<     {}: {:.3f}     >".format(text, value)).mouse_over:
        if is_mouse_down:
            value += (mouse_pos[0] - mouse_down_pos[0]) * step
            mouse_down_pos = mouse_pos

    reset_line()

    return value
