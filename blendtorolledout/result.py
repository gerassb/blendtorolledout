class Result:
    def __init__(self, is_ok: bool, value):
        self.is_ok = is_ok
        self.value = value

    def unwrap(self):
        assert self.is_ok, "Tried to unwrap an Err Result"
        return self.value

    def unwrap_err(self):
        assert not self.is_ok, "Tried to unwrap_err an Ok Result"
        return self.value


def Ok(value = None) -> Result:
    return Result(True, value)


def Err(value = None) -> Result:
    return Result(False, value)
